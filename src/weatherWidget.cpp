#include "weatherWidget.hpp"

// konstruktor hlavni zobrazovaci tridy
WeatherWidget::WeatherWidget(const QString &fileName, QMainWindow * parent) 
    : QMainWindow(parent)
{
    /** instance trid */
    filename = fileName;
    settingsDialog = new WeatherSettingsDialog(filename, this);

    weatherApi = new WeatherApi(this);    

    weatherTime = new WeatherTime(this);
    // -------

    // znacky
    flag = false;
    fWriteSettings = true;

    // volani funkci
    setMainWidgetLayout();
    createActions();
    createMenus();
    // -------

    /** nastaveni okna */
    message = tr("Vše připraveno");
    statusBar()->showMessage(message, 2000);

    setWindowTitle(tr("Počasí"));
    setMinimumSize(320, 480);
    //resize(640, 480);
    // -------

    readSettings(); // cteni poslediho nastaveni a nastaveni do puvodniho nastaveni 
    //this->setStyleSheet(QString("QMainWindow {background-color: %1}").arg(settingsDialog->getColor()));
    if (weatherApi->lang == "") weatherApi->lang = "cz";
    if (weatherApi->units == "") weatherApi->units = "metric";
    settingsDialog->setVar(weatherApi->lang, weatherApi->units);
    settingsDialog->act();  // dodatecna aktualizace widgetu (bez ni nezustane zmenenny po restartu okna nastaveni)

    /** spojeni signalu a slotu */
    QObject::connect(bLoad, QPushButton::clicked, this, WeatherWidget::selectTown);
    QObject::connect(bReset, QPushButton::clicked, this, WeatherWidget::reset);
    QObject::connect(bFind, QPushButton::clicked, this, WeatherWidget::findTown);
    QObject::connect(bSave, QPushButton::clicked, this, WeatherWidget::save);

    timerActual = new QTimer(this);
    QObject::connect(timerActual, &QTimer::timeout, weatherTime, &WeatherTime::actualTime);
    timerActual->start(1000);

    QObject::connect(timerActual, &QTimer::timeout, this, &WeatherWidget::timeChange);
    
    timerTakCesnetSync = new QTimer(this);
    QObject::connect(timerTakCesnetSync, &QTimer::timeout, weatherTime, &WeatherTime::takCesnet);
    timerTakCesnetSync->start(10000);

    timerTakCesnet = new QTimer(this);
    QObject::connect(timerTakCesnet, &QTimer::timeout, weatherTime, &WeatherTime::cesnetConnectDiff);
    timerTakCesnet->start(1000);
    
    QObject::connect(timerTakCesnet, &QTimer::timeout, this, &WeatherWidget::timeChangeTak);
    
    timerOnPlace = new QTimer(this);
    QObject::connect(bLoad, &QPushButton::clicked, this, &WeatherWidget::timeOnPlace);
    QObject::connect(bFind, &QPushButton::clicked, this, &WeatherWidget::timeOnPlace);

    timerOnPlace->start(1000);

    QObject::connect(timerOnPlace, &QTimer::timeout, this, &WeatherWidget::timeChangeOnPlace);
    // -------
}

/* metoda pro nastaveni ui **/
void WeatherWidget::setMainWidgetLayout()
{
    /** ovladaci widget */


    QLabel * lSelect = new QLabel(this);
    lSelect->setText("Vyberte město");

    listOfPlaces = new QStringList();
    this->listOfPlaces->prepend("-->Vyberte ze seznamu jedno z měst<--");
    listOfPlacesCombo = new QComboBox(this);
    listOfPlacesCombo->addItems(listOfPlaces->toList());
    listOfPlacesCombo->setStatusTip(tr("Možnost vyhledání místa ze seznamu. Vyberte město a klikněte na tlačítko Načíst."));
    listOfPlacesCombo->setToolTip(tr("Vyberte prosím jedno z míst."));
    bLoad = new QPushButton(this);
    bLoad->setText("Načíst");
    bLoad->setMaximumWidth(70);
    bLoad->setStatusTip(tr("Vyhledá se vybrané místo."));

    QLabel * lFind = new QLabel(this);
    lFind->setText("Vyhledejte město: ");
    txt = new QLineEdit(this);
    txt->setMaximumWidth(listOfPlacesCombo->width());
    txt->setStatusTip(tr("Zadejte existuící místo (město)."));
    txt->setToolTip(tr("Zadejte prosím existující město."));
    bFind = new QPushButton(this);
    bFind->setText("Vyhledat");
    bFind->setMaximumWidth(70);
    bFind->setStatusTip(tr("Vyhledá se zadané město."));

    bReset = new QPushButton(this);
    bReset->setText("Reset");
    bReset->setMaximumWidth(70);
    bReset->setStatusTip(tr("Vymaže a obnoví výběr."));
    bSave = new QPushButton(this);
    bSave->setText("Uložit data");
    bSave->setMaximumWidth(70);
    bSave->setStatusTip(tr("Uloží data do databáze."));

    /** informacni widget s mistem a casem */
    lTown = new QLabel(this);
    lTown->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTown->setObjectName(QString::fromUtf8("lPlaceTime"));

#if !STYLE_LOAD
    lTown->setStyleSheet("background-color: white; border-style: outset; border-width: 2px; border-color: beige;");
#endif

    lTown->setMaximumHeight(25);
    lTown->setText("Vyhledané město: ---");

    lTimeDesc = new QLabel(this);
    lTimeDesc->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTimeDesc->setText("Aktuální čas:");
    lTimeDesc->setWordWrap(true);

    lTime = new QLabel(this);
    lTime->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTime->setText("\t---");
    lTime->setWordWrap(true);
    lTime->setObjectName(QString::fromUtf8("lTimes"));
    
    lTimeTakDesc = new QLabel(this);
    lTimeTakDesc->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTimeTakDesc->setText("Aktuální čas z atomových hodin:");
    lTimeTakDesc->setWordWrap(true);

    lTimeTak = new QLabel(this);
    lTimeTak->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTimeTak->setText("---");
    lTimeTak->setWordWrap(true);
    lTimeTak->setObjectName(QString::fromUtf8("lTimes"));

    lTimeOnPlaceDesc = new QLabel(this);
    lTimeOnPlaceDesc->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTimeOnPlaceDesc->setText("Aktuální čas na daném místě:");
    lTimeOnPlaceDesc->setWordWrap(true);

    lTimeOnPlace = new QLabel(this);
    lTimeOnPlace->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTimeOnPlace->setText("---");
    lTimeOnPlace->setWordWrap(true);
    lTimeOnPlace->setObjectName(QString::fromUtf8("lTimes"));

    /** widgety pro vypis */
    QLabel * lTemp = new QLabel(this);
    lTemp->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTemp->setText("Teplota:");
    lTemp->setObjectName(QString::fromUtf8("lDescription"));
    lTemperature = new QLabel(this);
    lTemperature->setObjectName(QString::fromUtf8("lData"));
    lTemperature->setStatusTip(tr("Vyhledaná teplota s nastavenými jednotkami"));

#if !STYLE_LOAD
    lTemperature->setStyleSheet("background-color: yellow; border-style: outset; border-width: 2px; border-color: beige;");
#endif

    lTemperature->setText("---");
    lTemperature->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

    QLabel * lHum = new QLabel(this);
    lHum->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lHum->setText("Vlhkost:");
    lHum->setObjectName(QString::fromUtf8("lDescription"));
    lHumidity = new QLabel(this);
    lHumidity->setObjectName(QString::fromUtf8("lData"));
    lHumidity->setStatusTip(tr("Vyhledaná vlhkost"));

#if !STYLE_LOAD
    lHumidity->setStyleSheet("background-color: yellow; border-style: outset; border-width: 2px; border-color: beige;");
#endif

    lHumidity->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lHumidity->setText("---");

    QLabel * lPress = new QLabel(this);
    lPress->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lPress->setText("Tlak:");
    lPress->setObjectName(QString::fromUtf8("lDescription"));
    lPressure = new QLabel(this);
    lPressure->setObjectName(QString::fromUtf8("lData"));
    lPressure->setStatusTip(tr("Vyhledaný tlak."));

#if !STYLE_LOAD
    lPressure->setStyleSheet("background-color: yellow; border-style: outset; border-width: 2px; border-color: beige;");
#endif

    lPressure->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lPressure->setText("---");

    /** widgety pro podnebi -> aby se neposouvaly ostatni widgety (kvuli icon) */
    lClimateState = new QLabel(this);
    lClimateState->setMinimumHeight(30);
    lClimateState->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lClimateState->setObjectName(QString::fromUtf8("lDescription2"));
    lClimateState->setText("Podnebí: \t---");
    lClimate = new QLabel(this);
    lClimate->setObjectName(QString::fromUtf8("lDataClimate"));
    lClimate->setStatusTip(tr("Vyhledané podnebí s ikonou."));

#if !STYLE_LOAD
    lClimate->setStyleSheet("background-color: #99FF99; border-style: outset; border-width: 2px; border-color: beige;");
#endif

    lClimate->setText("---");
    lClimate->setMinimumHeight(30);
    lClimate->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

    /** layout */
    mainWidget = new QWidget(this);

    // top
    QWidget * top = new QWidget;
    top->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout * vboxTop = new QVBoxLayout();
    QHBoxLayout * hboxControl_load = new QHBoxLayout();
    hboxControl_load->addWidget(lSelect);
    hboxControl_load->addWidget(listOfPlacesCombo);    
    hboxControl_load->addWidget(bLoad);

    vboxTop->addLayout(hboxControl_load);

    QHBoxLayout * hboxControl_find = new QHBoxLayout();
    hboxControl_find->addWidget(lFind);
    hboxControl_find->addWidget(txt);    
    hboxControl_find->addWidget(bFind);

    vboxTop->addLayout(hboxControl_find);

    QHBoxLayout * hboxControl_other = new QHBoxLayout();
    hboxControl_other->addWidget(bReset);
    hboxControl_other->addWidget(bSave);
    hboxControl_other->setAlignment(Qt::AlignRight);

    vboxTop->addLayout(hboxControl_other);

    top->setLayout(vboxTop);

    // middle misto a cas
    QWidget * middle_found = new QWidget;
    middle_found->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout * vboxFoundAndTime = new QVBoxLayout();
    vboxFoundAndTime->addWidget(lTown);

    QHBoxLayout * hboxTime = new QHBoxLayout();
    QVBoxLayout * vboxTimeLeft = new QVBoxLayout();
    QVBoxLayout * vboxTimeMiddle = new QVBoxLayout();
    QVBoxLayout * vboxTimeRight = new QVBoxLayout();

    vboxTimeLeft->addWidget(lTimeDesc);
    vboxTimeLeft->addWidget(lTime);
    vboxTimeMiddle->addWidget(lTimeTakDesc);
    vboxTimeMiddle->addWidget(lTimeTak);
    vboxTimeRight->addWidget(lTimeOnPlaceDesc);
    vboxTimeRight->addWidget(lTimeOnPlace);

    hboxTime->addLayout(vboxTimeLeft);
    hboxTime->addLayout(vboxTimeMiddle);
    hboxTime->addLayout(vboxTimeRight);

    vboxFoundAndTime->addLayout(hboxTime);

    middle_found->setLayout(vboxFoundAndTime);

    // middle data
    QWidget * middle_data = new QWidget;
    middle_data->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout * vboxData1 = new QVBoxLayout();
    QVBoxLayout * vboxData2 = new QVBoxLayout();
    QHBoxLayout * hboxData = new QHBoxLayout();

    vboxData1->addWidget(lTemp);
    vboxData2->addWidget(lTemperature);
    vboxData1->addWidget(lHum);
    vboxData2->addWidget(lHumidity);
    vboxData1->addWidget(lPress);
    vboxData2->addWidget(lPressure);

    hboxData->addLayout(vboxData1);
    hboxData->addLayout(vboxData2);

    middle_data->setLayout(hboxData);

    // bottom
    QWidget * bottom = new QWidget(this);
    QHBoxLayout * hboxClimate = new QHBoxLayout();
    hboxClimate->addWidget(lClimateState);
    hboxClimate->addWidget(lClimate);

    bottom->setLayout(hboxClimate);

    // main
    QVBoxLayout * vboxMain = new QVBoxLayout();
    vboxMain->setContentsMargins(5, 5, 5, 5);
    vboxMain->addWidget(top);
    vboxMain->addWidget(middle_found);
    vboxMain->addWidget(middle_data);
    vboxMain->addWidget(bottom);

    mainWidget->setLayout(vboxMain);

    this->setCentralWidget(mainWidget);
    // -------
}

void WeatherWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.exec(event->globalPos());
}

/** metoda pro vytvoreni akci pro menu */
void WeatherWidget::createActions()
{
    aboutA = new QAction(tr("&O projektu"), this);
    aboutA->setStatusTip(tr("Další informace o projektu."));
    QWidget::connect(aboutA, &QAction::triggered, this, &WeatherWidget::info);

    aboutQtA = new QAction(tr("&O frameworku Qt"), this);
    aboutQtA->setStatusTip(tr("Informace o Qt."));
    QWidget::connect(aboutQtA, &QAction::triggered, qApp, &QApplication::aboutQt);

    quitA = new QAction(tr("&Ukončit"), this);
    quitA->setStatusTip(tr("Ukončit aplikaci."));
    QWidget::connect(quitA, &QAction::triggered, this, &QApplication::quit);

    settingsA = new QAction(tr("&Možnosti"), this);
    settingsA->setStatusTip(tr("Možnosti nastavení, vlastnosti a nástroje."));
    QWidget::connect(settingsA, &QAction::triggered, this, &WeatherWidget::settings);

    timezonesA = new QAction(tr("&Časová pásma"), this);
    timezonesA->setStatusTip(tr("Zobrazit časová pásma na mapě."));
    QWidget::connect(timezonesA, &QAction::triggered, weatherTime, &WeatherTime::actualTimezoneForMap);
    QWidget::connect(timezonesA, &QAction::triggered, this, &WeatherWidget::timezones);
    
    viewStBarA = new QAction(tr("&Zobrazit stavovou lištu"));
    viewStBarA->setStatusTip(tr("Zobrazit / skrýt stavovou lištu."));
    viewStBarA->setCheckable(true);
    viewStBarA->setChecked(true);
    QWidget::connect(viewStBarA, &QAction::triggered, this, [this] () {
        if (viewStBarA->isChecked())
        {
            message = tr("Stavová lišta zobrazena.");
            statusBar()->showMessage(message, 2000);
            statusBar()->show();
        }
        else
            statusBar()->hide();
    });

    forecastA = new QAction(tr("&Předpověď počasí"));
    forecastA->setStatusTip(tr("Zobrazit okno s předpovědí."));
    QWidget::connect(forecastA, &QAction::triggered, this, &WeatherWidget::forecast);
}

/** metoda pro vytvoreni menu */
void WeatherWidget::createMenus()
{
    settingsM = menuBar()->addMenu(tr("&Možnosti"));
    settingsM->setMinimumWidth(190);
    settingsM->addAction(settingsA);
    settingsM->addSeparator();
    settingsM->addAction(viewStBarA);

    otherM = menuBar()->addMenu(tr("&Další"));
    otherM->setMinimumWidth(150);
    otherM->addAction(forecastA);
    otherM->addSeparator();
    otherM->addAction(timezonesA);
    otherM->addAction(aboutA);
    otherM->addAction(aboutQtA);
    otherM->addSeparator();
    otherM->addAction(quitA);
}

/** metoda pro vypsani informaci o projektu */
void WeatherWidget::info()
{
    /*QWidget * infoWindow  = new QWidget();
    QVBoxLayout * vboxL = new QVBoxLayout(infoWindow);
    QPushButton * b2 = new QPushButton();
    b2->setText("My button");
    vboxL->addWidget(b2);
    infoWindow->setLayout(vboxL);
    infoWindow->setWindowTitle("Informace o projektu");
    infoWindow->show();*/

    QString aboutProjectText;
    aboutProjectText = QMessageBox::tr(
        "<h3>WeatherApp</h3>"
        "<p>Projekt byl vytvořen jako semestrální práce pro předmět Praktické programování v C/C++ vyučovaným na "
        " ČVUT FEL.</p>"
        "<p>Akademický rok 2020/21, letní semestr.</p>"
        );
    QMessageBox msgBox;;
    msgBox.setWindowTitle(tr("O Projektu"));
    msgBox.setText(aboutProjectText);
    msgBox.exec();
}

/** (slot)metoda pro vytvoreni noveho okna s casovymi pásmy */
void WeatherWidget::timezones()
{
    QWidget * timezoneWindow = new QWidget();
    QVBoxLayout * vboxImage = new QVBoxLayout();
    
    try {
        QString file (":/icons/World_Time_Zones_Map.png");
        QPixmap image(file);
        QLabel * lImage = new QLabel();

        if (!image.isNull())
        {
            QPixmap imageScaled = image.scaled(1200, 640, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            
            lImage->setPixmap(imageScaled);
        }
        else
            lImage->setText(tr("Nepovedlo se načíst obrázek časových pásem."));
        
        vboxImage->addWidget(lImage);
    } catch (...) {
        qDebug() << "Nepovedlo se nacist obrazek casovych pasem.";
    }
    
    QHBoxLayout * hboxText = new QHBoxLayout();
    QLabel * lText = new QLabel();
    lText->setText(tr("<p><b>Časový posuv</b> po celém světě je zobrazen na této mapě.</p>"));

    lActualTimezone = new QLabel();
    lActualTimezone->setText(QString(tr("<b>Aktuální časový posun</b>: %1")).arg(weatherTime->actualTimezone));

    hboxText->addWidget(lText);
    hboxText->addWidget(lActualTimezone);
    hboxText->setAlignment(lText, Qt::AlignLeft | Qt::AlignHCenter);
    hboxText->setAlignment(lActualTimezone, Qt::AlignRight | Qt::AlignHCenter);

    vboxImage->addLayout(hboxText);
    
    lActualPos = new QLabel();
    lActualPos->setText(QString(tr("<b>Aktuální pozice</b>: %1")).arg(weatherTime->actualPos));
    vboxImage->addWidget(lActualPos);
    vboxImage->setAlignment(lActualPos, Qt::AlignRight | Qt::AlignHCenter);

    timezoneWindow->setLayout(vboxImage);
    timezoneWindow->setWindowTitle("Časová pásma");
    timezoneWindow->setFixedSize(1240, 680);
    //timezoneWindow->setSizeConstraint(QLayout::SetFixedSize);
    timezoneWindow->show();
}

/** metoda pro obnoveni puvodniho nastaveni */
void WeatherWidget::defaultSettings()
{
    lTown->setText("Vyhledané město: ---");
    lTemperature->setText("---");
    lHumidity->setText("---");
    lPressure->setText("---");
    lClimateState->setText("Podnebí: \t---");
    lClimate->setText("---");
    txt->clear();
    listOfPlacesCombo->setCurrentIndex(0);

    weatherApi->timezoneState = false;
    lTimeOnPlaceDesc->setText("UTC Časový posun (hod):\t--\nAktuální čas na daném místě:");
    lTimeOnPlace->setText("---");

    message = tr("Obnoveno");
    statusBar()->showMessage(message, 2000);
}

/** (slot)metoda pro ziskani dat z comboBoxu */
void WeatherWidget::selectTown()
{
    if (listOfPlacesCombo->currentText() != "-->Vyberte ze seznamu jedno z měst<--")
    { 
        QString town = listOfPlacesCombo->currentText().replace(QString(" "), QString("+"));
        if (weatherApi->fetch(town))
        {
            message = tr("Data vyhledána.");
            statusBar()->showMessage(message, 2000);
            setDataInUi();
            flag = true;
        }
        else
        {
            message = tr("Err 5: Data nevyhledána. Nebylo naleznuto zadané místo.");
            statusBar()->showMessage(message, 2000);
            defaultSettings();
            flag = false;
        }
    }
    else
    {
        QMessageBox msgBox;
        message = tr("Err 1: Vyberte prosím volbu města.");
        statusBar()->showMessage(message, 2000);
        msgBox.setWindowTitle("Upozornění");
        msgBox.setText("Vyberte prosím volbu města.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        defaultSettings();
        flag = false;
    }
}

/** metoda pro nastaveni ziskani dat na ui prvky */
void WeatherWidget::setDataInUi()
{
    lTemperature->setText(QString::number(weatherApi->temperature) + weatherApi->unitPostfix);
    lHumidity->setText(QString::number(weatherApi->humidity) + " %");
    lPressure->setText(QString::number(weatherApi->pressure) + " hPa");
    lClimateState->setText("Podnebí: \t" + weatherApi->weather);

    QString town = weatherApi->town;
    town.replace(QString("+"), QString(" ")); 
    lTown->setText("Vyhledané město: " + town + ", " + weatherApi->country);

    if (!(weatherApi->climateImage.isNull()))
    {
        lClimate->clear();
        lClimate->setPixmap(weatherApi->climateImage);
    }
    else
    {
        lClimate->setText("---");
        message = tr("Err 2: Ikona podnebí nebyla nalezena.");
        statusBar()->showMessage(message, 2000);
    }

    txt->clear();
    listOfPlacesCombo->setCurrentIndex(0);
}

/** (slot)metoda pro naleznuti mesta po zadani do textoveho vstupu */
void WeatherWidget::findTown()
{
    QString input(txt->text());
    if (input != "")
    {
        input.replace(QString(" "), QString("+")); 
        if (weatherApi->fetch(input))
        {
            message = tr("Data vyhledána.");
            statusBar()->showMessage(message, 2000);
            setDataInUi();
            flag = true;
        }
        else
        {
            message = tr("Err 5: Data nevyhledána. Nebylo naleznuto zadané místo.");
            statusBar()->showMessage(message, 2000);
            defaultSettings();
            flag = false;
        }
    }
    else
     {
        QMessageBox msgBox;
        message = tr("Err 3: Vyberte prosím volbu města.");
        statusBar()->showMessage(message, 2000);
        msgBox.setWindowTitle("Upozornění");
        msgBox.setText("Vyberte prosím volbu města.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        defaultSettings();
        flag = false;
    }
}

/** (slot)metoda pro obnoveni puvodniho nastaveni */
void WeatherWidget::reset()
{
    defaultSettings();
}

/** (slot)metoda pro ulozeni aktualnich dat do databaze */
void WeatherWidget::save()
{
    if (lTown->text() !=  "Vyhledané město: ---")
    {
        if (weatherApi->fetch(weatherApi->town, true))
        {
            message = tr("Data uložena.");
            statusBar()->showMessage(message, 2000);
        }
        else
        {
            message = tr("Err 4: Data neuložena. Nebylo naleznuto zadané místo.");
            statusBar()->showMessage(message, 2000);
            defaultSettings();
            flag = false;
        }
    }
    else 
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Upozornění");
        msgBox.setText("Nevybráno žádné město.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        message = tr("Err 4: Data neuložena.");
        statusBar()->showMessage(message, 2000);
    }
}

/** (slot)metoda pro zjisteni aktualniho casu */
void WeatherWidget::timeChange()
{
    lTime->setText(weatherTime->timeStr);
}

/** (slot)metoda pro zjisteni aktualniho casu z atomovych hodin ze serveru tak */
void WeatherWidget::timeChangeTak()
{
    if (weatherTime->timeStrTak == "---")
    {
        lTimeTak->setStyleSheet("color: red;");
        lTimeTak->setText(weatherTime->timeStrTak);
        setWindowTitle("Počasí - Offline");
    }
    else
    {
        lTimeTak->setStyleSheet("color: black;");
        lTimeTak->setText(weatherTime->timeStrTak);
        setWindowTitle("Počasí - Online");
    }
}

/** metoda pro vytvoreni okna s nastavenimi */
void WeatherWidget::settings()
{
    settingsDialog->act();  // aktualizace            
    settingsDialog->setWindowModality(Qt::WindowModal); // zajisteni, aby se nedalo pouzivat okno rodice (hlavni ui)
    settingsDialog->show();
    
    fWriteSettings = settingsDialog->exec();    // ziskani navratove hodnoty z dialogoveho okna (zda uzivatel ulozil nastaveni ci nikoli)
    // podminka pro zjisteni ukonceni okna dialogu moznosti
    if (fWriteSettings)
    {
        try {
            // [1]
            weatherApi->units = settingsDialog->getUnits();

            if (weatherApi->units == "metric")
                weatherApi->unitPostfix = "°C";
            else if (weatherApi->units == "imperial")
                weatherApi->unitPostfix = "°F";
            else 
                weatherApi->unitPostfix = "°C";
            
            weatherApi->lang = settingsDialog->getLang();

            // pri nacteni prazdneho retezce, nactou se vyhozi nastaveni
            if (weatherApi->lang == "") weatherApi->lang = "cz";
            if (weatherApi->units == "") weatherApi->units = "metric";
            // [-]

            // [2]
            fTimeVisible = settingsDialog->getVisibleTime();
            setNewStatus();
            // [-]

            // [3]
            listOfPlaces = settingsDialog->getList();
            while (listOfPlaces->indexOf("-->Vyberte ze seznamu jedno z měst<--") != -1)
            {
                int index = listOfPlaces->indexOf("-->Vyberte ze seznamu jedno z měst<--");
                listOfPlaces->removeAt(index);
            }

            listOfPlaces->prepend("-->Vyberte ze seznamu jedno z měst<--");
            listOfPlacesCombo->clear();
            listOfPlacesCombo->addItems(listOfPlaces->toList());
            // [-]

            // [4]
            this->setStyleSheet(QString("QMainWindow {background-color: %1}").arg(settingsDialog->getColor()));
            // [-]

            // [5]
            weatherApi->forecastLength = settingsDialog->getForecastLength();
            weatherApi->weatherForecastFetch(weatherApi->lat, weatherApi->lon);
            // [-]

            message = tr("Vše připraveno");
            statusBar()->showMessage(message, 2000);

            // zapis do registru posledniho ulozeneho nastaveni
            writeSettings();
        }
        catch(...) {
            QMessageBox::warning(this, tr("Chyba"),
                            tr("Nepovedlo se uložit nastavení, nastavení je vráceno do výchozího stavu."),
                            QMessageBox::Ok);
            weatherApi->units = "metric";
            weatherApi->lang = "cz";
            settingsDialog->errSave();

            message = tr("Nastaveni obnoveno po chybe ulozeni");
            statusBar()->showMessage(message, 2000);
        }
    }

    settingsDialog->setWindowNameSaveState(fWriteSettings);
}

/** (slot)metoda pro nove okno s predpovedi pocasi */
void WeatherWidget::forecast()
{
    //weatherApi->forecastLength = settingsDialog->getForecastLength();
    ForecastDialog * forecastDialog = new ForecastDialog(weatherApi->temperatureCast.count(), this);

    /*if (weatherApi->weatherForecastFetch(weatherApi->lat, weatherApi->lon))
    {*/
        if (weatherApi->temperatureCast.count() != 0 && weatherApi->town != "")
        {
            forecastDialog->setData(weatherApi->temperatureCast, weatherApi->weatherCast, weatherTime->timeForForecastConv(weatherApi->hoursCast), weatherApi->hoursCastPixmap(), weatherApi->unitPostfix);
            forecastDialog->setData(weatherApi->town, weatherApi->country, weatherApi->loadPicture(weatherApi->icon, QSize(120, 120)), weatherApi->temperature, weatherApi->weather, weatherApi->unitPostfix);
            forecastDialog->setData(weatherTime->currentTimeForCast());
            forecastDialog->show();
            forecastDialog->exec();
        }
        else
            QMessageBox::warning(this, tr("Chyba"),
                    tr("Nepovedlo se zobrazit předpověď. Nejdříve vyhledejte město, ve kterém chcete předpověď načíst."),
                    QMessageBox::Ok);
    /*}
    else
    {
        message = tr("Err 6: Předpověď nenalezena.");
        statusBar()->showMessage(message, 2000);
    }*/
}

/** metoda pro zmenu v ui */
void WeatherWidget::setNewStatus()
{
    if (fTimeVisible[0])
    {
        lTimeDesc->setVisible(true);
        lTime->setVisible(true);
    }
    else
    {
        lTimeDesc->setVisible(false);
        lTime->setVisible(false);
    }

    if (fTimeVisible[1])
    {
        lTimeTakDesc->setVisible(true);
        lTimeTak->setVisible(true);
    }
    else
    {
        lTimeTakDesc->setVisible(false);
        lTimeTak->setVisible(false);
    }

    lTime->repaint();
    lTimeTak->repaint();
}

/** (slot)metoda propojena s tlacitkem na vyhledani mista */
void WeatherWidget::timeOnPlace()
{
    if (flag)
    {
        weatherApi->timezoneState = true;
        weatherTime->timeOnPlace(weatherApi->timezone, true);
    }
    else weatherApi->timezoneState = false;
}

/** (slot)metoda pro zjisteni aktualniho casu v danem meste a vypis do labelu */
void WeatherWidget::timeChangeOnPlace()
{
    if (weatherApi->timezoneState)
    {   
        QString timeShift = (weatherApi->timezone > 0) ? QString("+%1").arg(weatherApi->timezone / 3600) : QString("%1").arg(weatherApi->timezone / 3600);  // vypis i se znamenky jako prefix
        lTimeOnPlaceDesc->setText(QString("UTC Časový posun (hod):\t%1\nAktuální čas na daném místě:").arg(timeShift));
        lTimeOnPlace->setText(weatherTime->timeStrOnPlace);
        weatherTime->timeOnPlace(weatherApi->timezone, true);
    }
    else
    {
        lTimeOnPlace->setText("---");
    }
}

/** metoda pro cteni nastaveni, ktere se ulozilo pred poslednim zavrenim okna */
void WeatherWidget::readSettings()
{
    QSettings * settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    // obnoveni puvodniho nastaveni okna aplikace - pozice, rozmery
    const QByteArray geometry = settings->value("geometry", QByteArray()).toByteArray();
    if (geometry.isEmpty())
    {
        // zmena velikosti okna v zavislosti na moznostech zobrazovaciho zarizeni
        const QRect availableGeometry = screen()->availableGeometry();
        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2, (availableGeometry.height() - height() / 2));
    }
    else
    {
        restoreGeometry(geometry);
    }

    // obnoveni puvodniho nastaveni API promennych
    weatherApi->lang = settings->value("values/lang").toString();
    weatherApi->units = settings->value("values/units").toString();
    weatherApi->unitPostfix = settings->value("values/unitPostfix").toString();

    // obnoveni puvodniho nastaveni moznosti zobrazovani ukoncovaciho dialogoveho okna
    if (settings->value("values/showAgainExitWindow") == "true")
    {
        settingsDialog->setExitMsgBox(true);
    }
    else
        settingsDialog->setExitMsgBox(false);

    // obnoveni puvodniho nastaveni viditelnosti casu
    if (settings->value("flags/actualTimeVisible").toString() == "true")
    {
        fTimeVisible.append(true);
    }
    else
        fTimeVisible.append(false);

    if (settings->value("flags/actualTimeTakVisible").toString() == "true")
    {
        fTimeVisible.append(true);
    }
    else
        fTimeVisible.append(false);

    settingsDialog->setVisibleTime(fTimeVisible);

    setNewStatus();

    // obnoveni puvodniho nastaveni seznamu mist
    QStringList strList = settings->value("values/listOfPlaces").value<QStringList>();
    settingsDialog->setListOfPlaces(strList);
    listOfPlaces = settingsDialog->getList();
    listOfPlaces->prepend("-->Vyberte ze seznamu jedno z měst<--");
    listOfPlacesCombo->clear();
    listOfPlacesCombo->addItems(listOfPlaces->toList());

    // obnoveni barvy pozadi
    settingsDialog->setColor(settings->value("values/bgColor").toString());
    this->setStyleSheet(QString("QMainWindow {background-color: %1}").arg(settingsDialog->getColor()));

    // obnoveni stylu;
    settingsDialog->setStyle(settings->value("app/style").toInt());
    settingsDialog->setWindowNameSaveState(true);

    // obnoveni delky predpovedi
    settingsDialog->setForecastLength(settings->value("values/forecastLength").toInt());
    weatherApi->forecastLength = settingsDialog->getForecastLength();
}

/** metoda pro zapis aktualniho nastaveni pri ukonceni aplikace */
void WeatherWidget::writeSettings()
{
    /*
        Mapa nastaveni:
        |___
        |   |__ geometry
        |   |__ values
        |   |   |_ lang
        |   |   |_ units
        |   |   |_ unitPostfix
        |   |   |_ showAgainExitWindow
        |   |   |_ listOfPlaces []
        |   |   |_ bgColor
        |   |   |_ forecastLength
        |   |
        |   |__ flags
        |       |_ actualTimeVisible
        |       |_ actualTimeTakVisible
        |__ app
            |_ style
    */

    QSettings * settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings->setValue("geometry", saveGeometry());
    settings->setValue("values/lang", weatherApi->lang);
    settings->setValue("values/units", weatherApi->units);
    settings->setValue("values/unitPostfix", weatherApi->unitPostfix);
    settings->setValue("values/showAgainExitWindow", settingsDialog->getExitState());
    settings->setValue("flags/actualTimeVisible", fTimeVisible[0]);
    settings->setValue("flags/actualTimeTakVisible", fTimeVisible[1]);
    settings->setValue("values/listOfPlaces", QVariant::fromValue(settingsDialog->getList()->toList()));
    settings->setValue("values/bgColor", settingsDialog->getColor());
    settings->setValue("app/style", settingsDialog->getStyle());
    settings->setValue("values/forecastLength", weatherApi->forecastLength);
}

/** metoda pro potvrzeni ukonceni aplikace, jeji navratova hodnota se vyuzije v prepisujici funkci pro ulozeni nastaveni */
bool WeatherWidget::maybeClose()
{
    // puvodni reseni
    /*const QMessageBox::StandardButton ret = QMessageBox::warning(this, tr("Počasí"),
                            tr("Opravdu chcete ukoncit aplikaci?"),
                            QMessageBox::Yes | QMessageBox::No);
                            

    switch (ret) 
    {
        case QMessageBox::Yes:
            return true;
        case QMessageBox::No:
            return false;
        default:
            break;
    }
    return true;*/
    QSettings * settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    // vlastni reseni pomoci custom dialogoveho okna
    if (!(settings->value("values/showAgainExitWindow") == "true"))
    {
        QApplication::beep();

        ExitDialog * exit = new ExitDialog(this);
        exit->setWindowModality(Qt::WindowModal);
        exit->show();
        bool res = exit->exec();
        settingsDialog->setExitMsgBox(exit->checked);
        return res;
    }
    else return true;
}

/** reimplementovana metoda, ktera detekuje ukonceni aplikace a ulozeni posledniho nastaveni */
void WeatherWidget::closeEvent(QCloseEvent * event)
{
    if (maybeClose()) {
        if (fWriteSettings)
            writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}