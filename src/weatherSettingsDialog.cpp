#include "weatherSettingsDialog.hpp"

/** konstruktor hlavni tridy pro dialogove okno  */
WeatherSettingsDialog::WeatherSettingsDialog(const QString &fileName, QWidget * parent) 
    : QDialog(parent)
{
    QFileInfo fileInfo(fileName);

    // vytvareni instanci trid jednotlivych karet okna moznosti
    generalTab = new GeneralTab(fileInfo);
    weatherApiTab = new WeatherApiTab();
    unitConverterTab = new UnitConverterTab();

    // vytvoreni tabWidgetu
    tabWidget = new QTabWidget();
    tabWidget->addTab(generalTab, tr("Obecné"));
    tabWidget->addTab(weatherApiTab, tr("API nastavení"));
    tabWidget->addTab(unitConverterTab, tr("Převodník teplot"));
    
    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    /** layout */
    QVBoxLayout * vboxMain = new QVBoxLayout();
    vboxMain->addWidget(tabWidget);
    vboxMain->addWidget(buttonBox);
    setLayout(vboxMain);
    // -------

    setWindowTitle(tr("Možnosti"));
    layout()->setSizeConstraint(QLayout::SetFixedSize);

    // propojeni signalu se sloty
    QObject::connect(buttonBox, &QDialogButtonBox::accepted, this, [=] () {
        bool result = true;
        this->done(result);
    });

    QObject::connect(buttonBox, &QDialogButtonBox::rejected, this, [=] () {
        bool result = false;
        this->done(result);
    });
}

QString WeatherSettingsDialog::getUnits()
{
    return weatherApiTab->units;
}

QString WeatherSettingsDialog::getLang()
{
    return weatherApiTab->lang;
}

bool WeatherSettingsDialog::getExitState()
{
    return generalTab->showExitWindow;
}

void WeatherSettingsDialog::setVar(QString setLang, QString setUnits)
{
    weatherApiTab->setLangFromOut(setLang);
    weatherApiTab->setUnitsFromOut(setUnits);
}

void WeatherSettingsDialog::setExitMsgBox(bool setShowExitWindow)
{
    generalTab->setExitWindowFromOut(setShowExitWindow);
}

QVector<bool> WeatherSettingsDialog::getVisibleTime()
{
    return generalTab->fTimeVisible;
}

void WeatherSettingsDialog::setVisibleTime(QVector<bool> timeVectorVisible)
{
    generalTab->setVisibleTimeFromOut(timeVectorVisible);
}

void WeatherSettingsDialog::setListOfPlaces(QStringList setListOfPlaces)
{
    generalTab->setListOfPlacesFromOut(setListOfPlaces);
}

QStringList * WeatherSettingsDialog::getList()
{
    return generalTab->listOfPlacesOnTab;
}

QString WeatherSettingsDialog::getColor()
{
    return generalTab->color;
}

void WeatherSettingsDialog::errSave()
{
    generalTab->reset();
    weatherApiTab->reset();
    unitConverterTab->clear();
}

void WeatherSettingsDialog::setColor(QString color)
{
    generalTab->setColorFromOut(color);
}

void WeatherSettingsDialog::act()
{
    generalTab->actWidgets();
}

int WeatherSettingsDialog::getStyle()
{
    return generalTab->actStyle_index;
}

void WeatherSettingsDialog::setStyle(int style_index)
{
    generalTab->setStyleFromOut(style_index);
}

void WeatherSettingsDialog::setWindowNameSaveState(bool state)
{
    if (state)
        setWindowTitle(tr("Možnosti - Uloženo"));
    else
        setWindowTitle(tr("Možnosti - Neuloženo"));
}

int WeatherSettingsDialog::getForecastLength()
{
    return weatherApiTab->forecastLength;
}

void WeatherSettingsDialog::setForecastLength(int length)
{
    weatherApiTab->setForecastLengthFromOut(length);
}   

/** konstruktor tridy s hlavnim nastavenim aplikace */
GeneralTab::GeneralTab(const QFileInfo &fileInfo, QWidget * parent)
    : QWidget(parent)
{
    cActualTime = new QCheckBox(tr("&Aktuální čas"));
    cActualTimeTak = new QCheckBox(tr("&Aktuální čas z atomových hodin"));

    cActualTime->setChecked(true);
    cActualTimeTak->setChecked(true);

    cMsgbox = new QCheckBox(tr("&Nezobrazovat potvrzovací okno pro ukončování aplikace."));
    cMsgbox->setChecked(false);

    listOfPlacesOnTab = new QStringList();
    listOfPlacesOnTab->append("Praha");
    listOfPlacesOnTab->append("Brno");
    listOfPlacesOnTab->append("Ostrava");
    listOfPlacesOnTab->append("Sázava");

    QLabel * lSelect = new QLabel(tr("Vyberte ze seznamu nebo napište do název města."));
    editListOfPlacesCombo = new QComboBox();
    editListOfPlacesCombo->setEditable(true);
    editListOfPlacesCombo->clear();
    editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
    editListOfPlacesCombo->setCurrentText("");
    QPushButton * popupButton = new QPushButton(tr("..."));

    plusA = new QAction(tr("+"));
    minusA = new QAction(tr("-"));
    rstA = new QAction(tr("RST"));

    QMenu * menu = new QMenu(this);
    //menu->addAction(tr("<<"));
    //menu->addAction(tr("+"));
    //menu->addAction(tr("-"));
    //menu->addAction(tr(">>"));
    menu->addAction(plusA);
    menu->addAction(minusA);
    menu->addSeparator();
    menu->addAction(rstA);
    popupButton->setMenu(menu);

    QPushButton * bColor = new QPushButton(tr("&Vybrat barvu pozadí hlavní aplikace"));
    lColor = new QLabel();
    lColor->setFrameStyle(QFrame::Sunken | QFrame::Panel);
    lColor->setMaximumHeight(20);
    lColor->setText(color);
    lColor->setPalette(QPalette(color));
    lColor->setAutoFillBackground(true);

    QLabel * lStyle = new QLabel(tr("Změna motivu aplikace:"));
    listOfStyles = new QStringList();
    listOfStyles->append("Windows");
    listOfStyles->append("Fusion");
    stylesCombo = new QComboBox();
    stylesCombo->addItems(listOfStyles->toList());

    QLabel * lPath = new QLabel(tr("Cesta:"));
    QLabel * lValuePath = new QLabel(fileInfo.absoluteFilePath());
    lValuePath->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    lValuePath->setTextInteractionFlags(Qt::TextSelectableByMouse);
    lValuePath->setMaximumHeight(25);
    
    bReset = new QPushButton(tr("&Obnovit původní nastavení"));
    bReset->setMaximumSize(210, 25);
    bReset->setEnabled(false);

    /** layout */
    QVBoxLayout * vboxMain = new QVBoxLayout();
    QHBoxLayout * hboxMain = new QHBoxLayout();
    
    QVBoxLayout * vboxLeft = new QVBoxLayout();
    QVBoxLayout * vboxRight = new QVBoxLayout();

    QGroupBox * gLabelView = new QGroupBox(tr("Zviditelnit zobrazování časů"));

    QVBoxLayout * vboxLabelView = new QVBoxLayout();
    vboxLabelView->addWidget(cActualTime);
    vboxLabelView->addWidget(cActualTimeTak);
    gLabelView->setLayout(vboxLabelView);

    vboxLeft->addWidget(gLabelView);

    QGroupBox * gOther = new QGroupBox(tr("Další možnosti"));
    QGroupBox * gEditCombo = new QGroupBox(tr("Změna uložených destinací v seznamu"));
    gEditCombo->setFlat(true);
    gEditCombo->setAlignment(Qt::AlignCenter);

    QGridLayout * grPlaces = new QGridLayout();

    grPlaces->addWidget(lSelect, 0, 0);
    grPlaces->addWidget(editListOfPlacesCombo, 1, 0);
    grPlaces->addWidget(popupButton, 1, 1);
    gEditCombo->setLayout(grPlaces);

    QGroupBox * gDesign = new QGroupBox(tr("Změna designu"));
    gDesign->setFlat(true);
    gDesign->setAlignment(Qt::AlignCenter);

    QGridLayout * grDesign = new QGridLayout();
    QHBoxLayout * hboxPath = new QHBoxLayout();
    grDesign->addWidget(bColor, 0, 0);
    grDesign->addWidget(lColor, 0, 1);
    grDesign->addWidget(lStyle, 1, 0);
    grDesign->addWidget(stylesCombo, 1, 1);

    gDesign->setLayout(grDesign);

    QVBoxLayout * vboxOther = new QVBoxLayout();
    vboxOther->addWidget(cMsgbox);
    vboxOther->addSpacing(10);
    vboxOther->addWidget(gEditCombo);
    vboxOther->addSpacing(10);
    vboxOther->addWidget(gDesign);
    gOther->setLayout(vboxOther);
    vboxRight->addWidget(gOther);

    hboxPath->addWidget(lPath, 0, Qt::AlignLeft);
    hboxPath->addWidget(lValuePath, 0, Qt::AlignRight);

    gDesign->setLayout(grDesign);
    hboxMain->addLayout(vboxLeft);
    hboxMain->addLayout(vboxRight);
    vboxMain->addLayout(hboxMain);
    vboxMain->addLayout(hboxPath);
    vboxMain->addWidget(bReset, 0, Qt::AlignRight);

    setLayout(vboxMain);
    // -------

    isDefault();

    // propojeni signalu se sloty
    QObject::connect(cMsgbox, &QCheckBox::stateChanged, this, &GeneralTab::setExitState);
    QObject::connect(bColor, &QPushButton::clicked, this, &GeneralTab::setColor);
    QObject::connect(bReset, &QPushButton::clicked, this, &GeneralTab::reset);

    QObject::connect(cActualTime, &QCheckBox::stateChanged, this, [this] () {
        if (cActualTime->isChecked()) fTimeVisible[0] = true;
        else fTimeVisible[0] = false;

        bReset->setEnabled(true);
    });

    QObject::connect(cActualTimeTak, &QCheckBox::stateChanged, this, [this] () {
        if (cActualTimeTak->isChecked()) fTimeVisible[1] = true;
        else fTimeVisible[1] = false;

        bReset->setEnabled(true);
    });

    QWidget::connect(plusA, &QAction::triggered, this, GeneralTab::plus);
    QWidget::connect(minusA, &QAction::triggered, this, GeneralTab::minus);

    QWidget::connect(rstA, &QAction::triggered, this, [this] () {
        editListOfPlacesCombo->clear();
        listOfPlacesOnTab->clear();
        listOfPlacesOnTab->append("Praha");
        listOfPlacesOnTab->append("Brno");
        listOfPlacesOnTab->append("Ostrava");
        listOfPlacesOnTab->append("Sázava");
        editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
        editListOfPlacesCombo->setCurrentText("");

        bReset->setEnabled(true);
    });

    QWidget::connect(stylesCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), [=] (int index) {
        if (index == 0)
            qApp->setStyle("windowsvista");
        else
            qApp->setStyle(listOfStyles->at(index));

        actStyle_index = index;
        actWidgets();

        bReset->setEnabled(true);
    });
}

void GeneralTab::plus()
{
    QString str = editListOfPlacesCombo->currentText();

    // dalo by se take vyuzit nejakym zpusobem https://doc.qt.io/qt-5/qregexpvalidator.html - overeni vstupu
    // overeni, zda neni zadano prazdne pole
    if (editListOfPlacesCombo->currentText().isEmpty())
    {
        QMessageBox::warning(this, tr("Chyba"),
            tr("Nebylo zadáno žádné město."),
            QMessageBox::Ok);
        return;
    }

    // overeni, zda neni v seznamu nasledujici string, pokud ano, smaze se
    while (listOfPlacesOnTab->indexOf("-->Vyberte ze seznamu jedno z měst<--") != -1)
    {
        int index = listOfPlacesOnTab->indexOf("-->Vyberte ze seznamu jedno z měst<--");
        listOfPlacesOnTab->removeAt(index);
    }

    // pridani mesta do seznamu
    if (listOfPlacesOnTab->indexOf(str) == -1)
        listOfPlacesOnTab->append(str);
    else
    {
        QMessageBox::warning(this, tr("Chyba"),
                        QString(tr("Zadané město (%1) se již nachází v seznamu.")).arg(str),
                        QMessageBox::Ok);
    }

    editListOfPlacesCombo->clear();
    editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
    editListOfPlacesCombo->setCurrentText("");

    bReset->setEnabled(true);
}

void GeneralTab::minus()
{
    QString str = editListOfPlacesCombo->currentText();

    // overeni, zda neni zadano prazdne pole
    if (editListOfPlacesCombo->currentText().isEmpty())
    {
        QMessageBox::warning(this, tr("Chyba"),
            tr("Nebylo zadáno žádné město."),
            QMessageBox::Ok);
        
        return;
    }

    // overeni, zda neni v seznamu nasledujici string, pokud ano, smaze se
    while (listOfPlacesOnTab->indexOf("-->Vyberte ze seznamu jedno z měst<--") != -1)
    {
        int index = listOfPlacesOnTab->indexOf("-->Vyberte ze seznamu jedno z měst<--");
        listOfPlacesOnTab->removeAt(index);
    }

    // odebrani mesta ze seznamu
    if (listOfPlacesOnTab->indexOf(str) != -1)
    {
        int index = listOfPlacesOnTab->indexOf(str);
        listOfPlacesOnTab->removeAt(index);
    }
    else
    {
        QMessageBox::warning(this, tr("Chyba"),
                        QString(tr("Zadané město (%1) nebylo nalezeno v seznamu.")).arg(str),
                        QMessageBox::Ok);
    }

    editListOfPlacesCombo->clear();
    editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
    editListOfPlacesCombo->setCurrentText("");

    bReset->setEnabled(true);
}

void GeneralTab::setExitWindowFromOut(bool setShowExitWindow)
{
    if (setShowExitWindow) cMsgbox->setChecked(true);
    else cMsgbox->setChecked(false);
    showExitWindow = setShowExitWindow;
}

void GeneralTab::setExitState()
{
    if (cMsgbox->isChecked()) showExitWindow = true;
    else showExitWindow = false;

    bReset->setEnabled(true);
}

void GeneralTab::setColor()
{
    const QColorDialog::ColorDialogOptions options = QFlag(QColorDialog::ShowAlphaChannel);
    const QColor color = QColorDialog::getColor(Qt::green, this, "Vyberte barvu", options);

    // pokud je kod barvy platny nebo se nejedna o cernou barvu, pak nastav barvu pozadi dle vyberu
    if (color.isValid() && (color.name() != "#000000"))
    {
        lColor->setText(color.name());
        lColor->setPalette(QPalette(color));
        lColor->setAutoFillBackground(true);
        this->color = color.name();
    }
    else
    {
        this->color = "#f0f0f0";
        QMessageBox::warning(this, tr("Chyba"),
                            QString(tr("Zadaná barva není platná. Byla nastavena výchozí (%1).")).arg(this->color),
                            QMessageBox::Ok);
        lColor->setText(this->color);
        lColor->setPalette(QPalette(this->color));
        lColor->setAutoFillBackground(true);
    }

    bReset->setEnabled(true);
}

void GeneralTab::reset()
{
    cMsgbox->setChecked(false);

    cActualTime->setChecked(true);
    cActualTimeTak->setChecked(true);

    editListOfPlacesCombo->clear();
    listOfPlacesOnTab->clear();
    listOfPlacesOnTab->append("Praha");
    listOfPlacesOnTab->append("Brno");
    listOfPlacesOnTab->append("Ostrava");
    listOfPlacesOnTab->append("Sázava");
    editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
    editListOfPlacesCombo->setCurrentText("");

    color = "#f0f0f0";
    lColor->setText(color);
    lColor->setPalette(QPalette(color));
    lColor->setAutoFillBackground(true);

    stylesCombo->setCurrentIndex(0);
    
    bReset->setEnabled(false);
}

void GeneralTab::setVisibleTimeFromOut(QVector<bool> timeVectorVisible)
{
    fTimeVisible = timeVectorVisible;
    cActualTime->setChecked(fTimeVisible[0]);
    cActualTimeTak->setChecked(fTimeVisible[1]);    
}

void GeneralTab::setListOfPlacesFromOut(QStringList setListOfPlaces)
{
    listOfPlacesOnTab->clear();

    // pokud je seznam ziskany z registru pomoci QSettings nulovy, pak se naplni vychozimi prvky
    if (setListOfPlaces.count() == 0)
    {
        listOfPlacesOnTab->append("Praha");
        listOfPlacesOnTab->append("Brno");
        listOfPlacesOnTab->append("Ostrava");
        listOfPlacesOnTab->append("Sázava");
        return;
    }

    // jednotlive se projdou vsechny prvky ziskaneho pole a ulozi se do hlavniho seznamu mist
    foreach (const QString &item, setListOfPlaces)
    {
        if (item == "-->Vyberte ze seznamu jedno z měst<--")
            continue;
        listOfPlacesOnTab->append(item);
    }

    // obnoveni a aktualizace
    editListOfPlacesCombo->clear();
    editListOfPlacesCombo->addItems(listOfPlacesOnTab->toList());
    editListOfPlacesCombo->setCurrentText("");
}

void GeneralTab::setColorFromOut(QString colorFromOut)
{
    const QColor color = colorFromOut;
    if (color.isValid())
    {
        lColor->setText(color.name());
        lColor->setPalette(QPalette(color));
        lColor->setAutoFillBackground(true);
        this->color = color.name();
    }
    else
    {
        this->color = "#f0f0f0";
    }
}

void GeneralTab::actWidgets()
{
    lColor->setText(color);
    lColor->setPalette(QPalette(color));
    lColor->setAutoFillBackground(true);

    stylesCombo->setCurrentIndex(actStyle_index);
}

void GeneralTab::setStyleFromOut(int styleFromOut_index)
{
    if (styleFromOut_index == 0)
        actStyle_index = styleFromOut_index;
    else
        actStyle_index = 1;

    stylesCombo->setCurrentIndex(actStyle_index);
}

void GeneralTab::isDefault()
{
    if (cMsgbox->isChecked())
        bReset->setEnabled(true);
    else if (!cActualTime->isChecked() || !cActualTimeTak->isChecked())
        bReset->setEnabled(true);
    else if (listOfPlacesOnTab->at(0) != "Praha" || listOfPlacesOnTab->at(1) != "Brno" ||
            listOfPlacesOnTab->at(2) != "Ostrava" || listOfPlacesOnTab->at(3) != "Sázava")
        bReset->setEnabled(true);
    else if (color != "#f0f0f0")
        bReset->setEnabled(true);
    else if (stylesCombo->currentIndex() != 0)
        bReset->setEnabled(true);
}

/** konstruktor tridy pro nastaveni API */
WeatherApiTab::WeatherApiTab(QWidget * parent)
    : QWidget(parent)
{
    forecastLength = 8;

    rMeter = new QRadioButton(tr("&Metrické"));
    rImperial = new QRadioButton(tr("&Imperiální"));

    rMeter->setChecked(true);

    QStringList * listOfLanguages = new QStringList();
    listOfLanguages->append("Česky");
    listOfLanguages->append("Anglicky");
    listOfLanguages->append("Slovensky");
    listOfLanguages->append("Polsky");
    listOfLanguages->append("Německy");
    listOfLanguagesCombo = new QComboBox();
    listOfLanguagesCombo->addItems(listOfLanguages->toList());

    QLabel * lForecastLength = new QLabel(tr("Počet hodin pro předpověď:"));
    spinBox = new QSpinBox();
    spinBox->setRange(2, 12);
    spinBox->setValue(8);
    spinBox->setMaximumWidth(50);

    bReset = new QPushButton(tr("&Obnovit původní nastavení"));
    bReset->setMaximumSize(210, 25);
    bReset->setEnabled(false);

    /** layout */
    QVBoxLayout * vboxMain = new QVBoxLayout();
    QHBoxLayout * hboxMain = new QHBoxLayout();
    
    QVBoxLayout * vboxLeft = new QVBoxLayout();
    QVBoxLayout * vboxRight = new QVBoxLayout();

    QGroupBox * gUnits = new QGroupBox(tr("Jednotky"));
    QHBoxLayout * hboxUnits = new QHBoxLayout();
    hboxUnits->addWidget(rMeter, 0, Qt::AlignCenter);
    hboxUnits->addWidget(rImperial, 0, Qt::AlignCenter);
    gUnits->setLayout(hboxUnits);
    
    vboxLeft->addWidget(gUnits);

    QGroupBox * gLanguage = new QGroupBox(tr("Jazyk vyhledávání měst"));
    QVBoxLayout * vboxLanguages = new QVBoxLayout();
    vboxLanguages->addWidget(listOfLanguagesCombo, 0, Qt::AlignCenter);
    gLanguage->setLayout(vboxLanguages);
    
    vboxRight->addWidget(gLanguage);

    hboxMain->addLayout(vboxLeft);
    hboxMain->addLayout(vboxRight);

    QGroupBox * grForecastLength = new QGroupBox(tr("Možnosti pro předpověď"));
    QHBoxLayout * hboxForecastLength = new QHBoxLayout();
    hboxForecastLength->addWidget(lForecastLength, 0, Qt::AlignCenter);
    hboxForecastLength->addWidget(spinBox, 0, Qt::AlignCenter);
    grForecastLength->setLayout(hboxForecastLength);

    vboxMain->addLayout(hboxMain);
    vboxMain->addWidget(grForecastLength);
    vboxMain->addWidget(bReset, 0, Qt::AlignRight);

    setLayout(vboxMain);
    // -------

    isDefault();

    // propojeni signalu se sloty
    // https://medium.com/genymobile/how-c-lambda-expressions-can-improve-your-qt-code-8cd524f4ed9f
    QObject::connect(rMeter, &QRadioButton::toggled, [this] () {
        units = "metric";
        bReset->setEnabled(true);
    });

    QObject::connect(rImperial, &QRadioButton::toggled, [this] () {
        units = "imperial";
        bReset->setEnabled(true);
    });

    QObject::connect(listOfLanguagesCombo, &QComboBox::currentTextChanged, this, &WeatherApiTab::setLang);
    QObject::connect(bReset, &QPushButton::clicked, this, &WeatherApiTab::reset);

    QObject::connect(spinBox, &QSpinBox::valueChanged, this, [this] () {
        forecastLength = spinBox->value();
        bReset->setEnabled(true);
    });
}

void WeatherApiTab::setLang()
{
    if (listOfLanguagesCombo->currentText() == "Český") lang = "cz";
    else if (listOfLanguagesCombo->currentText() == "Anglicky") lang = "en";
    else if (listOfLanguagesCombo->currentText() == "Slovensky") lang = "sk";
    else if (listOfLanguagesCombo->currentText() == "Polsky") lang = "pl";
    else if (listOfLanguagesCombo->currentText() == "Německy") lang = "de";
    else lang = "cz";

    bReset->setEnabled(true);
}

void WeatherApiTab::setUnitsFromOut(QString setUnits)
{
    if (setUnits == "metric") rMeter->setChecked(true);
    else rImperial->setChecked(true);
}

void WeatherApiTab::setLangFromOut(QString setLang)
{
    //https://stackoverflow.com/questions/4340415/qcombobox-set-selected-item-based-on-the-items-data
    // reseni jazyku by se dalo take udelat tak, ze se namapuje nejaky objekt s nazvem a zkratkou
    if (setLang == "cz") listOfLanguagesCombo->setCurrentIndex(0);
    else if (setLang == "en") listOfLanguagesCombo->setCurrentIndex(1);
    else if (setLang == "sk") listOfLanguagesCombo->setCurrentIndex(2);
    else if (setLang == "pl") listOfLanguagesCombo->setCurrentIndex(3);
    else if (setLang == "de") listOfLanguagesCombo->setCurrentIndex(4);
}

void WeatherApiTab::reset()
{
    rMeter->setChecked(true);
    listOfLanguagesCombo->setCurrentIndex(0);

    bReset->setEnabled(false);

    spinBox->setValue(8);
}

void WeatherApiTab::isDefault()
{
    if (!rMeter->isChecked())
        bReset->setEnabled(true);
    else if (listOfLanguagesCombo->currentIndex() != 0)
        bReset->setEnabled(true);
    else if (spinBox->value() != 8)
        bReset->setEnabled(true);
}

void WeatherApiTab::setForecastLengthFromOut(int setForecastLength)
{
    forecastLength = setForecastLength;
    spinBox->setValue(forecastLength);
}

/** konstruktor tridy pro prevod jednotek */
UnitConverterTab::UnitConverterTab(QWidget * parent)
    : QWidget(parent)
{
    rCtoF = new QRadioButton(tr("&C -> F"));
    rFtoC = new QRadioButton(tr("&F -> C"));

    rCtoF->setChecked(true);

    QLabel * lIn = new QLabel(tr("Vstup"));
    QLabel * lOut = new QLabel(tr("Výstup"));
    LIn = new QLineEdit();
    LOut = new QLineEdit();
    LOut->setReadOnly(true);

    QLabel * lDecimal = new QLabel(tr("Počet desetinných míst:"));
    spinBox = new QSpinBox();
    spinBox->setRange(0, 6);
    spinBox->setValue(2);
    spinBox->setMaximumWidth(30);

    QPushButton * bConvert = new QPushButton("&Převést");
    QPushButton * bClear = new QPushButton("&Smazat");
    bConvert->setMaximumHeight(40);
    bClear->setMaximumHeight(30);

    /** layout */
    QHBoxLayout * hboxMain = new QHBoxLayout();
    QVBoxLayout * vboxLeft = new QVBoxLayout();

    QGroupBox * groupBox = new QGroupBox(tr("Směr převodu"));
    QHBoxLayout * hboxConv = new QHBoxLayout();
    hboxConv->addWidget(rCtoF, 0, Qt::AlignCenter);
    hboxConv->addWidget(rFtoC, 0, Qt::AlignCenter);
    groupBox->setLayout(hboxConv);

    QGridLayout * gridLayout = new QGridLayout();
    gridLayout->addWidget(lIn, 0, 0);
    gridLayout->addWidget(lOut, 0, 1);
    gridLayout->addWidget(LIn, 1, 0);
    gridLayout->addWidget(LOut, 1, 1);

    QHBoxLayout * hboxDecimal = new QHBoxLayout();
    hboxDecimal->addWidget(lDecimal, 0, Qt::AlignCenter);
    hboxDecimal->addWidget(spinBox, 0, Qt::AlignCenter);

    QHBoxLayout * hboxButtons = new QHBoxLayout();
    hboxButtons->addWidget(bConvert);
    hboxButtons->addWidget(bClear);

    vboxLeft->addWidget(groupBox);
    vboxLeft->addLayout(gridLayout);
    vboxLeft->addLayout(hboxDecimal);
    vboxLeft->addLayout(hboxButtons);

    hboxMain->addLayout(vboxLeft);

    setLayout(hboxMain);
    // -------

    QObject::connect(rCtoF, &QRadioButton::released, this, &UnitConverterTab::clearOut);
    QObject::connect(rFtoC, &QRadioButton::released, this, &UnitConverterTab::clearOut);
    QObject::connect(bConvert, &QPushButton::clicked, this, &UnitConverterTab::convert);
    QObject::connect(bClear, &QPushButton::clicked, this, &UnitConverterTab::clear);
}

void UnitConverterTab::clear()
{
    LIn->clear();
    LOut->clear();
}

void UnitConverterTab::clearOut()
{
    LOut->clear();
}

void UnitConverterTab::convert()
{
    QString in(LIn->text());
    bool ok;
    int inInt = in.toInt(&ok, 10);
    Q_UNUSED(inInt);

    // kontrola, zda jsou na vstupu pouze cisla
    if (!ok)
    {
        QMessageBox::warning(this, tr("Chyba"),
                            tr("Zkontrolujte prosím vstupní teplotu, zda se jedná o číslo."),
                            QMessageBox::Ok);
        LIn->clear();
        return;
    }

    float celsius, fahrenheit;
    QString out;
    
    if (rCtoF->isChecked())
    {
        celsius = LIn->text().toFloat();

        fahrenheit = (celsius * 9.0) / 5.0 + 32;
        out = QString::number(fahrenheit, 'f', spinBox->value());
    }
    else
    {
        fahrenheit = LIn->text().toFloat();

        celsius = 5 * (fahrenheit - 32) / 9;

        out = QString::number(celsius, 'f', spinBox->value());
    }

    LOut->setText(out);
}

/** konstruktor pro ukoncujici dialog */
ExitDialog::ExitDialog(QWidget * parent)
    : QDialog(parent)
{
    checked = false;

    QLabel * lImage = new QLabel();

    try {
        QString file (":/icons/qmessagebox-quest.png");
        QPixmap image(file);

        if (!image.isNull())
        {
            QPixmap imageScaled = image.scaled(40, 40, Qt::KeepAspectRatio, Qt::SmoothTransformation);

            lImage->setPixmap(imageScaled);
        }
        else
            lImage->setText(tr("<b>?</b>"));
    } catch (...) {
        qDebug() << "Nepovedlo se nacist obrazek otazky.";
        lImage->setText(tr("<b>?</b>"));
    }

    QLabel * lMsg = new QLabel(tr("Opravdu chcete ukoncit aplikaci?"));
    lMsg->setMinimumWidth(250);
    cAgain = new QCheckBox(tr("&Příště tuto zprávu nezobrazovat."));

    QFrame * line = new QFrame();
    line->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    /** layout */
    QVBoxLayout * vboxMain = new QVBoxLayout();
    QHBoxLayout * hboxTop = new QHBoxLayout();

    hboxTop->addWidget(lImage);
    hboxTop->addWidget(lMsg, 0, Qt::AlignRight);
    vboxMain->addLayout(hboxTop);

    vboxMain->setSpacing(10);
    vboxMain->addWidget(cAgain, 0, Qt::AlignLeft);
    vboxMain->addWidget(line);
    vboxMain->addWidget(buttonBox, 0, Qt::AlignRight);

    setLayout(vboxMain);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    // -------

    setWindowTitle("Počasí");

    // zbarveni okna kvuli obrazku - nyni splyva s pozadim
    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, Qt::white);
    setAutoFillBackground(true);
    setPalette(pal);

    // spojeni signalu se sloty
    QObject::connect(buttonBox, &QDialogButtonBox::accepted, this, [=] () {
        bool result = true;
        this->done(result);
    });
    QObject::connect(buttonBox, &QDialogButtonBox::rejected, this, [=] () {
        bool result = false;
        this->done(result);
    });
    QObject::connect(buttonBox, &QDialogButtonBox::accepted, this, &ExitDialog::exitYes);
}

void ExitDialog::exitYes()
{
    exitState = true;

    if (cAgain->isChecked()) checked = true;
    else checked = false;
}
