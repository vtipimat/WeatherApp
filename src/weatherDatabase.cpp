#include "weatherDatabase.hpp"

WeatherDatabase::WeatherDatabase(QWidget * parent) : QWidget(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE", "weatherConnection");    // QSQLITE - ovladac, driver
    db.setDatabaseName("weather.db");

    if (!db.open())
    {
        qDebug() << db.lastError().text();
    }

    QSqlQuery query(db);
    query.exec("CREATE TABLE IF NOT EXISTS pocasi (id INTEGER PRIMARY KEY, town VARCHAR (20), country VARCHAR (20), temperature REAL, humidity REAL, pressure REAL, icon VARCHAR (10), timestamp INTEGER)");
    query.clear();

    // zjisteni posledniho ID
    query.exec("SELECT MAX(id) FROM pocasi");
    if (query.next())
    {
        ID = query.value(0).toInt();
    }
    query.clear();
}

void WeatherDatabase::saveInDatabase(QString town, QString country, double temperature, double humidity, double pressure, QString icon, unsigned int timestamp)
{
    QString sql = QString("INSERT INTO pocasi (id, town, country, temperature, humidity, pressure, icon, timestamp) VALUES (%1, '%2', '%3', %4, %5, %6, '%7', %8)")
                        .arg(++ID).arg(town).arg(country).arg(temperature).arg(humidity).arg(pressure).arg(icon).arg(timestamp);   // apostrofy u %2, jinak to SQL bere jako nazev sloupce
    //QString sql = QString("INSERT INTO pocasi (town, country, temperature, humidity, pressure, icon, timestamp) VALUES ('%1', '%2', %3, %4, %5, '%6', %7)")
    //                                .arg(town).arg(country).arg(temperature).arg(humidity).arg(pressure).arg(icon).arg(timestamp);   // apostrofy u %2, jinak to SQL bere jako nazev sloupce
    
    QSqlQuery query(sql, db);   // poslani (zadani) dotazu
    query.clear();
}