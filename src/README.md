## Rozdělení souborů
Soubory definují třídy a metody k nim spojené. Rozdělení a popis souborů je následující:
- weatherApi.cpp - zajištění dat získaných z API
- weatherDatabase.hpp - zajištění ukládání dat do (SQLite) databáze
- weatherForecast.hpp - zajištění grafického zobrazení předpovědi počasí
- weatherSettingsDialog.hpp - zajištění dialogových oken možností a exit dialogu
- weatherTime.hpp - zajištění funkcionality s časem
- weatherWidget.hpp - zajištění propojení GUI a funkcionalit
