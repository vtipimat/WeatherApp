#include "weatherApi.hpp"

/** konstruktor tridy WeatherApi */
WeatherApi::WeatherApi(QWidget * parent) : QWidget(parent)
{
    key = "7953adfab5d9976697f108da19e69b14";
    lang = "cz";
    units = "metric";
    unitPostfix = "°C";
    timezoneState = false;
    forecastLength = 8;
    
    weatherDatabase = new WeatherDatabase();
}

/** hlavni metoda pro ziskani dat z API */
bool WeatherApi::fetch(QString town, bool saveStat)
{
    this->town = town;
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply *)), &eventLoop, SLOT(quit()));
    QNetworkRequest req (QUrl (QString("http://api.openweathermap.org/data/2.5/weather?q=%1&units=%2&appid=%3&lang=%4").arg(town).arg(units).arg(key).arg(lang)));	// dotaz
    QNetworkReply * reply = mgr.get(req);
    eventLoop.exec();   // smycka udalosti se ukonci

    if (reply->error() == QNetworkReply::NoError)
    {
        QString strReply = (QString)reply->readAll();
        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
        QJsonObject jsonObject = jsonResponse.object();

        // vycteni dat z API
        temperature = jsonObject.value(QString("main")).toObject()["temp"].toDouble();
        humidity = jsonObject.value(QString("main")).toObject()["humidity"].toDouble();
        pressure = jsonObject.value(QString("main")).toObject()["pressure"].toDouble();
        country = jsonObject.value(QString("sys")).toObject()["country"].toString();
        timestamp = jsonObject.value(QString("dt")).toInt();
        timezone = jsonObject.value(QString("timezone")).toInt();
        lon = jsonObject.value(QString("coord")).toObject()["lon"].toDouble();
        lat = jsonObject.value(QString("coord")).toObject()["lat"].toDouble();

        QJsonArray jsonArray = jsonObject["weather"].toArray();
        for (auto v : jsonArray)
        {
            QJsonObject obj = v.toObject();
            weather = obj["description"].toString();
            icon = obj["icon"].toString();
        }
        
        //loadPicture(icon, climateImage);
        climateImage = loadPicture(icon, QSize(60, 60));

        if (saveStat == true)
        {
            double temp;
            if (units == "imperial")
                temp = FtoC(temperature);
            else
                temp = temperature;

            weatherDatabase->saveInDatabase(town, country, temp, humidity, pressure, icon, timestamp);
        }

        weatherForecastFetch(lat, lon);

        delete reply;
        return true;
    }
    else
    {
        QMessageBox::warning(this, tr("Upozornění"),
                    tr("Nastala chyba.\nChyba mohla nastat na vstupu formuláře (neexistující místo) nebo zkontroluje připojení k internetu."),
                    QMessageBox::Ok);

        delete reply;
        return false;
    }
}

/** metoda pro nacteni ikony podnebi v dane oblasti */
/*void WeatherApi::loadPicture(QString icon, QPixmap climate)
{
    QUrl urlImg (QString("http://openweathermap.org/img/wn/%1@2x.png").arg(icon));    // http://openweathermap.org/img/wn/10d@2x.png"
    QNetworkAccessManager manager;
    QEventLoop loop;

    QNetworkReply * reply = manager.get(QNetworkRequest(urlImg));
    QObject::connect(reply, &QNetworkReply::finished, &loop, [&reply, &climate, &loop] () {
        if (reply->error() == QNetworkReply::NoError)
        {
            QByteArray pngData = reply->readAll();
            climate.loadFromData(pngData);
        }
        loop.quit();
    });
    loop.exec();

    QPixmap climateScaled = climate.scaled(60, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation);  // omezeni velikosti ikony, aby se vesla do ramecku bez jeho transfrormace

    climateImage = climateScaled;
}*/

/** metoda pro univerzalni ziskani pixmap ikon */
QPixmap WeatherApi::loadPicture(QString icon, QSize scaleSize)
{
    QPixmap climate;
    QUrl urlImg (QString("http://openweathermap.org/img/wn/%1@2x.png").arg(icon));    // http://openweathermap.org/img/wn/10d@2x.png"
    QNetworkAccessManager manager;
    QEventLoop loop;

    QNetworkReply * reply = manager.get(QNetworkRequest(urlImg));
    QObject::connect(reply, &QNetworkReply::finished, &loop, [&reply, &climate, &loop] () {
        if (reply->error() == QNetworkReply::NoError)
        {
            QByteArray pngData = reply->readAll();
            climate.loadFromData(pngData);
        }
        loop.quit();
    });
    loop.exec();

    QPixmap climateScaled = climate.scaled(scaleSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);  // omezeni velikosti ikony, aby se vesla do ramecku bez jeho transfrormace

    return climateScaled;
}

/** metoda pro prevod jednotek - sjednoceni jednotek zaznamu */
double WeatherApi::FtoC(double temp)
{
    return (5 * (temp - 32) / 9);
}

/** metoda pro ziskani dat pro predpoved */
bool WeatherApi::weatherForecastFetch(double lat, double lon)
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply *)), &eventLoop, SLOT(quit()));
    QNetworkRequest req (QUrl (QString("https://api.openweathermap.org/data/2.5/onecall?lat=%1&lon=%2&exclude=daily,current,minutely&units=%3&appid=%4&lang=%5").arg(lat).arg(lon).arg(units).arg(key).arg(lang)));	// dotaz
    QNetworkReply * reply = mgr.get(req);
    eventLoop.exec();   // smycka udalosti se ukonci

    //qDebug() << forecastLength << ", " << town;

    if (reply->error() == QNetworkReply::NoError)
    {
        QString strReply = (QString)reply->readAll();
        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
        QJsonObject jsonObject = jsonResponse.object();

        QJsonArray jsonArray = jsonObject["hourly"].toArray();
        
        temperatureCast.clear();
        weatherCast.clear();
        iconCast.clear();
        hoursCast.clear();

        int t = 0;
        for (auto v : jsonArray)
        {
            if (t == forecastLength)
                break;
            
            QJsonObject obj = v.toObject();
            temperatureCast.push_back(obj["temp"].toDouble());
            hoursCast.push_back(obj["dt"].toDouble());

            QJsonArray jsonArrayWeather = obj["weather"].toArray();
            for (auto w : jsonArrayWeather)
            {
                QJsonObject objWeather = w.toObject();
                weatherCast.push_back(objWeather["description"].toString());
                iconCast.push_back(objWeather["icon"].toString());
            }

            t++;
        }

        //qDebug() << temperatureCast.at(1) << "lat: " << lat << ", lon: " << lon << ", " << weatherCast.at(1) << ", " << iconCast.at(1);

        delete reply;
        return true;
    }
    else
    {
        QMessageBox::warning(this, tr("Upozornění"),
                    tr("Nastala chyba.\nChyba nastala při získávání dat pro předpověď. Zkontrolujte připojení k internetu."),
                    QMessageBox::Ok);

        delete reply;
        return false;
    }
}

/** metoda pro ziskani icon pro predpoved */
QVector<QPixmap> WeatherApi::hoursCastPixmap()
{
    QVector<QPixmap> iconsPixmap;

    for (int i = 0; i < iconCast.count(); i++)
    {
        iconsPixmap.append(loadPicture(iconCast.at(i), QSize(60, 60)));
    }

    return iconsPixmap;
}
