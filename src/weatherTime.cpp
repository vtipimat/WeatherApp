#include "weatherTime.hpp"

/** konstruktor pro tridu  obstaravajici cas*/
WeatherTime::WeatherTime(QWidget * parent) : QWidget(parent)
{
    flag = false;
}

/** (slot)metoda zajistujici spojeni se serverem cesnetu */
bool WeatherTime::takCesnet()
{
    try {
        if (!checkInternetAvailability())
            return false;

        QHostInfo HostInfo = QHostInfo::fromName("tak.cesnet.cz");	// pripojeni k casovemu serveru

        QUdpSocket * UdpSocket = new QUdpSocket();

        UdpSocket->connectToHost(QHostAddress(HostInfo.addresses().at(0)), 123);	// port 123, vytvoreni spojeni - privilegovany port

        char m[48] = {010, 0, 0, 0, 0, 0, 0, 0, 0};	// datagram (zprava) - ve spravnem formatu - 48 Bytu a na 1. miste je cislo 10 (hex 0x0B)

        UdpSocket->writeDatagram(m, sizeof(m), QHostAddress(HostInfo.addresses().at(0)), 123);	// poslu datagram na casovy server
        
        if (UdpSocket->waitForReadyRead()) {
            while (UdpSocket->hasPendingDatagrams()) {
                QByteArray data = UdpSocket->readAll();	// cteni vsech dat

                // little endian - na nejnizsi adrese nejmene dulezity byte (nebo tak nejak)
                // musi se prevest z big na little endian -> dalsi kod
                if (data.size() == 48) {
                    // pracujeme s big endianem a proto to chceme prevest na little endian
                    timeI = uchar(data[43]) + (uchar(data[42]) << 8) + (uchar(data[41]) << 16) + (uchar(data[40]) << 24/* = 3 byty posun zpet*/);	// zajimaji nas tyto byty
                    // u NTP serveru plyne cas od 1.1. 1900 a my to chceme odecist a pocitat s hodnoutou od zacatku PC casu (epochy)
                    timeI -= 2208988800;	// unixova epocha -> cas od nekdy v roce 1.1. 1970 - start pocitacoveho casu, a rozdil je toto cislo
                    timeTak = QDateTime::fromSecsSinceEpoch(timeI);
                    timeStrTak = timeTak.toString("hh:mm:ss");

                    QDateTime current = QDateTime::currentDateTime();
                    diff = current.msecsTo(timeTak);
                }
            }
        }

        return true;
    }
    catch(...) {
        qDebug() << "Chyba spojení.";
        return false;
    }
}

/** (slot)zajistujici rozdil casu z atomovych hodin a jejich periodickou synchronizaci */
void WeatherTime::cesnetConnectDiff()
{
    if (!flag)
    {
        if (takCesnet())
            flag = true;
    }
    
    if (checkInternetAvailability())
    {
        QDateTime current = QDateTime::currentDateTime();
        timeDiffTak = current.addMSecs(diff);
        timeStrTak = timeDiffTak.toString("hh:mm:ss");

        // animace dvojtecek
        QTime time = timeDiffTak.time();
        if ((time.second() % 2) == 0)
            timeStrTak[5] = ' ';
        if (time.second() == 00)
            timeStrTak[2] = ' ';
    }
    else
        timeStrTak = "---";
}

/** metoda pro zjisteni aktualniho casoveho pasma podle systemovych informaci */
void WeatherTime::actualTimezoneFind()
{
    QString actualTimezoneNoSubString = QDateTime::currentDateTime().toOffsetFromUtc(QDateTime::currentDateTime().offsetFromUtc()).toString(Qt::ISODate);
    actualTimezone = actualTimezoneNoSubString.right(6);

    int h, m;
    char sign;
    h = actualTimezone.mid(1, 2).toInt();
    m = actualTimezone.mid(3, 2).toInt();
    if (actualTimezone.left(1) == "+") sign = '+';
    else if (actualTimezone.left(1) == "-") sign = '-';
    else sign = ' ';

    actualTimezoneSec = (sign == '+') ? (h * 3600 + m * 60) : -(h * 3600 + m * 60);
}

/** (slot)metoda pro zjisteni aktualni pozice pro okno s mapou */
void WeatherTime::actualTimezoneForMap()
{
    actualTimezoneFind();

    QDateTime current = QDateTime::currentDateTime();
    current.setTimeSpec(Qt::LocalTime);
    actualPos = current.timeZoneAbbreviation();
}

/** (slot)metoda pro zjisteni aktualniho casu */
void WeatherTime::actualTime()
{
    // lze to udelat podobne i pomoci QDataTime - https://forum.qt.io/topic/49263/solved-how-to-get-the-difference-between-current-time-zone-and-utc
    time = QTime::currentTime();
    timeStr = time.toString("hh:mm:ss");

    if ((time.second() % 2) == 0)
        timeStr[5] = ' ';
    if (time.second() == 00)
        timeStr[2] = ' ';
}

/** metoda pro zjisteni aktualniho casu na danem miste */
void WeatherTime::timeOnPlace(int timezone, bool timezoneState)
{
    // https://greenwichmeantime.com/time-zone/definition/

    actualTimezoneFind();

    if (timezoneState)
    {
        QTime timeGMT = time.addSecs(-actualTimezoneSec);    	//Greenwich Mean Time (GMT) // casovy posun na vychozi cas, od ktereho se dale odpocitava (defaultni cas)
                                                                // z CR posun -> -7200
        QTime timeOnPlace = timeGMT.addSecs(timezone);  // prepocet na cas v danem miste
        timeStrOnPlace = timeOnPlace.toString("hh:mm:ss");

        if (timeOnPlace.second() % 2 == 0)
            timeStrOnPlace[5] = ' ';
        if (timeOnPlace.second() == 00)
            timeStrOnPlace[2] = ' ';
    }
    else
        timeStrOnPlace = "---";
}

/** metoda pro kontrolu spojeni s internetem */
bool WeatherTime::checkInternetAvailability()
{
    QTcpSocket * TcpSocket = new QTcpSocket(this);
    TcpSocket->connectToHost("www.google.com", 80);
    bool connected = TcpSocket->waitForConnected(30000); // ms

    if (!connected)
    {
        TcpSocket->abort();
        return false;
    }

    TcpSocket->close();
    return true;
}

/** metoda pro prepocet casu z casove znamky na predpoved */
QVector<QString> WeatherTime::timeForForecastConv(QVector<unsigned int> hoursTimeStamp)
{
    QVector<QString> timeForecast;

    for (int i = 0; i < hoursTimeStamp.count(); i++)
    {
        QDateTime hours = QDateTime::fromSecsSinceEpoch(hoursTimeStamp.at(i));
        //qDebug() << hours.toString("hh:mm:ss");
        timeForecast.append(hours.toString("hh:mm"));
    }

    return timeForecast;
}

/** metoda pro ziskani casu aktualni predpovedi */
QString WeatherTime::currentTimeForCast()
{
    return time.toString("hh:mm");
}
