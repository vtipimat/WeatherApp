#include "weatherForecast.hpp"

ForecastDialog::ForecastDialog(int dataCount, QWidget * parent)
    : QDialog(parent), length(dataCount)
{
    QHBoxLayout * hboxCast = new QHBoxLayout();

    lTown = new QLabel();
    lTown->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTown->setObjectName(QString::fromUtf8("lPlaceTime"));

    lCurrentWeather = new QLabel();
    lCurrentWeather->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    //lCurrentWeather->setObjectName(QString::fromUtf8("lDescription2"));

    lCurrentTime = new QLabel();
    lCurrentTime->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lCurrentTime->setObjectName(QString::fromUtf8("lTimeForecast"));

    lTemperature = new QLabel();
    lTemperature->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lTemperature->setObjectName(QString::fromUtf8("lData"));

    lDesc = new QLabel();
    lDesc->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
    lDesc->setObjectName(QString::fromUtf8("lDescription2"));

    /*QFrame * lineV = new QFrame();
    lineV->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    lineV->setFrameShape(QFrame::VLine);
    lineV->setFrameShadow(QFrame::Sunken);*/

    // cyklus pro inicializaci vektoru s labely
    for (int i = 0; i < length; i++)
    {
        QVBoxLayout * vbox = new QVBoxLayout();
        lWeather.append(new QLabel());
        lWeatherDesc.append(new QLabel());  
        lHours.append(new QLabel());
        lIcons.append(new QLabel());

        vbox->addWidget(lHours.at(i), 0, Qt::AlignCenter);
        vbox->addWidget(lIcons.at(i), 0, Qt::AlignCenter);
        vbox->addWidget(lWeather.at(i), 0, Qt::AlignCenter);
        vbox->addWidget(lWeatherDesc.at(i), 0, Qt::AlignCenter);
        hboxCast->addLayout(vbox);
        /*if (i != (length - 1))
            hboxCast->addWidget(lineV);*/
    }

    QFrame * lineH = new QFrame();
    lineH->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    lineH->setFrameShape(QFrame::HLine);
    lineH->setFrameShadow(QFrame::Sunken);

    /** layout */
    QVBoxLayout * vboxMain = new QVBoxLayout();

    QHBoxLayout * hboxCurrentData = new QHBoxLayout();
    QVBoxLayout * vboxCurrentDataText = new QVBoxLayout();

    hboxCurrentData->addWidget(lCurrentWeather);
    vboxCurrentDataText->addWidget(lCurrentTime);
    vboxCurrentDataText->addWidget(lTemperature);
    vboxCurrentDataText->addWidget(lDesc);
    hboxCurrentData->addLayout(vboxCurrentDataText);
    
    vboxMain->addWidget(lTown);
    vboxMain->addLayout(hboxCurrentData);
    vboxMain->addWidget(lineH);
    vboxMain->addLayout(hboxCast);

    setLayout(vboxMain);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    // -------

    setWindowTitle("Předpověď počasí");

    // zbarveni okna kvuli iconam
    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, QColor(153, 255, 153));
    setAutoFillBackground(true);
    setPalette(pal);
}

void ForecastDialog::setData(QVector<double> temp, QVector<QString> desc, QVector<QString> hours, QVector<QPixmap> icons, QString units)
{
    for (int i = 0; i < temp.count(); i++)
    {
        lWeather.at(i)->setText(QString("%1%2").arg(QString::number(temp.at(i), 'f', 2)).arg(units));
        lWeatherDesc.at(i)->setText(desc.at(i));
        lHours.at(i)->setText(hours.at(i));
        lIcons.at(i)->setPixmap(icons.at(i));
    }
}

void ForecastDialog::setData(QString place, QString country, QPixmap weatherPixmap, double currentTemp, QString desc, QString units)
{
    double secondTemp;
    QString secondUnits;

    if (units == "°C")
    {
        secondTemp = (currentTemp * 9.0) / 5.0 + 32;
        secondUnits = "°F";
    }
    else
    {
        secondTemp = 5 * (currentTemp - 32) / 9;
        secondUnits = "°C";
    }

    lTown->setText(QString("%1, %2").arg(place).arg(country));
    lCurrentWeather->setPixmap(weatherPixmap);
    if (units == "°C")
        lTemperature->setText(QString("%1%2 / %3%4").arg(QString::number(currentTemp, 'd', 2)).arg(units).arg(QString::number(secondTemp, 'd', 2)).arg(secondUnits));
    else
        lTemperature->setText(QString("%3%4 / %1%2").arg(QString::number(currentTemp, 'd', 2)).arg(units).arg(QString::number(secondTemp, 'd', 2)).arg(secondUnits));

    lDesc->setText(desc);
}

void ForecastDialog::setData(QString time)
{
    lCurrentTime->setText(time);
}