#include <QApplication>

#include "weatherWidget.hpp"
#include "myDefines.hpp"

int main(int argc, char *argv[])
{
    try {
        Q_INIT_RESOURCE(srcPaths);

        QApplication app(argc, argv);

        QString file (":/icons/11d.png");
        QIcon icon(file);
        app.setWindowIcon(icon);

        QCoreApplication::setApplicationName("WeatherApp");
        QCoreApplication::setOrganizationName("CVUT");

        file = ":/qss/style.qss";
        QFile styleFile(file);
        if (styleFile.open(QFile::ReadOnly))
        {
            QString styleSheet = QLatin1String(styleFile.readAll());
            app.setStyleSheet(styleSheet);
#undef STYLE_LOAD
#define STYLE_LOAD true
        }
        else
        {
#undef STYLE_LOAD
#define STYLE_LOAD false
        }
        
        QString fileName;
        if (argc >= 2)
            fileName = argv[1];
        else
            fileName = ".";

        WeatherWidget weather(fileName);
        weather.QWidget::show();

        QFontDatabase::addApplicationFont(":/fonts/orbitron-v17-latin-regular.ttf");

        return app.exec();
    }
    catch(...) {
        qDebug() << "Neznama chyba\n";
    }
}