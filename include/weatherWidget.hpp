#pragma once

#include <QtWidgets>

#include <QDebug>
#include <QTimer>
#include <QVariant>
#include <QSettings>
#include <QVector>
#include <QList>

/*
#include <QPushButton>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QStatusBar>
#include <QLineEdit>
#include <QGroupBox>
#include <QPixmap>
#include <QMessageBox>
#include <QMainWindow>
#include <QCheckBox>
#include <QRadioButton>
*/

//#include <QFile>

#include "weatherApi.hpp"
#include "weatherTime.hpp"
#include "weatherSettingsDialog.hpp"
#include "myDefines.hpp"
#include "weatherForecast.hpp"

QT_BEGIN_NAMESPACE
class QAction;
class QActionGroup;
class QLabel;
class QMenu;
QT_END_NAMESPACE

// hlavni vykreslovaci komponenta
class WeatherWidget : public QMainWindow
{
    Q_OBJECT // lepe funguji signaly a sloty

    QLabel * lTemperature;
    QLabel * lHumidity;
    QLabel * lPressure;
    QLabel * lClimate;
    QLabel * lTown;
    QLabel * lClimateState;
    QLabel * lBar1;
    QLabel * lBar2;
    QLabel * lTime;
    QLabel * lTimeTak;
    QLabel * lTimeOnPlace;
    QLabel * lActualPos;
    QLabel * lActualTimezone;
    QLabel * lTimeDesc;
    QLabel * lTimeTakDesc;
    QLabel * lTimeOnPlaceDesc;
    QComboBox * listOfPlacesCombo;
    QLineEdit * txt;
    QStatusBar * bar;
    QString message;
    QTimer * timerActual;
    QTimer * timerTakCesnetSync;
    QTimer * timerTakCesnet;    
    QTimer * timerOnPlace;
    QPushButton * bLoad;
    QPushButton * bReset;
    QPushButton * bFind;
    QPushButton * bSave;

    //QLCDNumber * LCDActTime;

    QVector<bool> fTimeVisible;
    QStringList * listOfPlaces;

    QWidget * mainWidget;
    WeatherApi * weatherApi;
    WeatherTime * weatherTime;
    WeatherSettingsDialog * settingsDialog;

    QString filename;

    bool flag;  // znacka, aby se pri vyhledani nebo vybrani s chybou nezobrazoval i cas na danem miste
    bool fWriteSettings;    // znacka vyjadrujici, aby se posledni ulozene nastaveni po tom, ce se okno s moznostmi ukoncilo, ulozilo, pokud uzivatel zadal "OK = accepted signal" -> true

public:
    WeatherWidget(const QString &fileName, QMainWindow * parent = 0);

protected:
    void contextMenuEvent(QContextMenuEvent * event) override;  // reimplementovani funkce pro meny
    void closeEvent(QCloseEvent * event) override; // reimplementovani funkce pro detekci ukonceni aplikace a ulozeni posledniho nastaveni

private:
    void setMainWidgetLayout(); // metoda pro nastaveni hlavniho layoutu
    void defaultSettings(); // metoda pro obnoveni puvodniho rozvrzeni (vynulovani)
    void setDataInUi(); // metoda pro vypsani vyhledanych dat
    void createActions();   // metoda pro vytvoreni akci v menubaru
    void createMenus(); // metoda pro vytvoreni meny
    void readSettings();    // metoda pro cteni nastaveni pred poslednim zavrenim
    void writeSettings();   // metoda pro zapis nastaveni pred zavrenim
    bool maybeClose();   // metoda pro potvrzeni zavreni
    void setNewStatus();    // metoda pro nastaveni zmen v ui

public slots:
    void selectTown();  // slot pro vyber mesta z comboBoxu
    void findTown();    // slot pro nalezeni mesta ze vstupu z textBoxu 
    void reset();   // slot pro obnoveni nastaveni
    void save();    // slot pro ulozeni dat do databaze
    
    // change = zmena pri timeoutech
    void timeChange();  // slot pro zmenu labelu pro aktualni cas
    void timeChangeTak();   // slot pro zmenu labelu pro aktualni cas ze serveru tak
    void timeOnPlace();     // slot  pro propojeni s tlacitky
    void timeChangeOnPlace();   // slot pro zmenu labalu pro aktualni cas na zjistovanem meste

    void info();    // slot pro otevreni noveho okna
    void settings();    // slot pro otevreni noveho okna s nastavenim
    void timezones();   // slot pro otevreni noveho okna pro zobrazeni pixmapy s casovymi pasmy
    void forecast();    // slot pro otevreni noveho okna s predpovedi pocasi

private:
    QMenu * otherM;
    QMenu * settingsM;
    QAction * aboutA;
    QAction * aboutQtA;
    QAction * quitA;
    QAction * settingsA;
    QAction * timezonesA;
    QAction * viewStBarA;
    QAction * forecastA;
};