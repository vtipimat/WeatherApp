#pragma once

#include <QWidget>
#include <QCoreApplication>
#include <QHostInfo>
#include <QUdpSocket>
#include <QDateTime>
#include <QTcpSocket>
#include <QTime>
#include <QVector>

// komponenta pro zajisteni informaci o casu
class WeatherTime : public QWidget
{
public:
    QString timeStr;    // aktualni cas
    QString timeStrTak; // aktualni cas z atomovych hodin
    QString timeStrOnPlace; // aktualni cas na miste
    QString actualPos;  // aktualni pozice
    QString actualTimezone; // aktualni casove pasmo
    int actualTimezoneSec;  // aktualni casove pasmo (int)
    
private:
    Q_OBJECT
    
    QTime time;
    unsigned long timeI;
    QDateTime timeTak;
    qint64 diff;
    QDateTime timeDiffTak;

    bool flag;  // priznak pro pocatecni inicializaci casoveho rozdilu

    void actualTimezoneFind();  // metoda, ktera zjistuje aktualni casovy posun
    bool checkInternetAvailability();   // metoda pro zjisteni stavu internetoveho spojeni (pomoci testovaciho spojeni TCP)

public:
    WeatherTime(QWidget * parent = 0);

    void timeOnPlace(int timezone, bool timezoneState); // funkce pro zjisteni casoveho rozdilu
    QVector<QString> timeForForecastConv(QVector<unsigned int> hoursTimeStamp); // metoda pro prepocet casu z casove znamky na predpoved
    QString currentTimeForCast();   // metoda pro ziskani casu aktualni predpovedi

public slots:
    bool takCesnet();   // slot pro zjisteni casu z atomovych hodin
    void actualTime();  // slot pro zjisteni aktualni casu systemu
    void cesnetConnectDiff();   // slot pro zjisteni casoveho rozdilu mezi systemovymi a atomovymi hodinami - aby se kazdou sekundu neptal program na spojeni a odpoved ze serveru, coz zpusobovalo zamrzani aplikace
    //void timeOnPlace(int timezone);
    void actualTimezoneForMap();    // slot pro zjisteni aktualniho casoveho posunu a pozice
};