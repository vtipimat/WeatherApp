#pragma once

#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QPixmap>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "weatherDatabase.hpp"

// komponenta pro zajisteni spojeni s API
class WeatherApi : public QWidget
{
public:
    double temperature, humidity, pressure;
    QString weather, icon, country, town;
    QPixmap climateImage;
    unsigned int timestamp;
    int timezone;
    bool timezoneState;
    double lon, lat;
    int forecastLength;

    WeatherDatabase * weatherDatabase;

    QString lang;
    QString units;
    QString unitPostfix;

    //double temperatureCast;
    QVector<double> temperatureCast;
    QVector<QString> weatherCast;
    QVector<QString> iconCast;
    //QVector<QPixmap> iconPixmapCast;
    QVector<unsigned int> hoursCast;

private:
    Q_OBJECT

    QString key;    // privatni klic pro API

public:
    WeatherApi(QWidget * parent = 0);

    bool fetch (QString town, bool saveStat = false);   // metoda pro ziskani dat
    //void loadPicture(QString icon, QPixmap climate);    // metoda pro vyhledani a ulozeni icony
    QPixmap loadPicture(QString icon, QSize scaleSize);    // metoda pro vyhledani a ulozeni icony
    double FtoC(double temp);   // metoda pro sjednoceni zaznamu do db (pouze v °C)

    bool weatherForecastFetch(double lat, double lon); // metoda pro ziskani dat pro predpved
    QVector<QPixmap> hoursCastPixmap(); // metoda pro ziskani icon pro predpoved
public slots:

};