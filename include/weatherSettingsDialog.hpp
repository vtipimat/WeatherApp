#pragma once

#include <QtWidgets>

#include <QColor>
#include <QPalette>
#include <QVector>
#include <QPixmap>

/*
#include <QDialog>
#include <QGroupBox>
#include <QRadioButton>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QColorDialog>
#include <QMenu>
#include <QMessageBox>
*/

QT_BEGIN_NAMESPACE
class QDialogButtonBox;
class QFileInfo;
class QTabWidget;
QT_END_NAMESPACE

/** trida pro kartu 1 - Obecne */
class GeneralTab : public QWidget
{
    Q_OBJECT

    QCheckBox * cMsgbox;
    QCheckBox * cActualTime;
    QCheckBox * cActualTimeTak;
    QLabel * lColor;
    QAction * plusA;
    QAction * minusA;
    QAction * rstA;
    QComboBox * editListOfPlacesCombo;
    QComboBox * stylesCombo;
    QPushButton * bReset;

public:
    explicit GeneralTab(const QFileInfo &fileInfo, QWidget * parent = 0);

    bool showExitWindow;
    QVector<bool> fTimeVisible;
    QStringList * listOfPlacesOnTab;
    QStringList * listOfStyles;
    QString color;
    int actStyle_index;

    void setExitWindowFromOut(bool setShowExitWindow);  // metoda pro nastaveni posledniho ukoncovaciho stavu pred poslednim ukonceni aplikace
    void setVisibleTimeFromOut(QVector<bool> timeVectorVisible); // metoda pro nastaveni posledniho stavu viditelnosti pred poslednim ukonceni aplikace
    void setListOfPlacesFromOut(QStringList setListOfPlaces); // metoda pro nastaveni posledniho stavu seznamu mist pred poslednim ukonceni aplikace
    void setColorFromOut(QString colorFromOut); // metoda pro nastaveni posledniho stavu barvy pozadi pred poslednim ukonceni aplikace
    void setStyleFromOut(int styleFromOut_index);   // metoda pro nastaveni posledniho stavu stylu aplikace pred poslednim ukoncemnim aplikace

    void actWidgets();  // metoda pro aktualizaci widgetu s barvou (jinak je cerna bez aktualizace)
    void isDefault();   // metoda pro kontrolu po spusteni aplikace, zda je nejake nastaveni jine nez vychozi, pokud ano, pak se zpristupni tlacitko reset

public slots:
    void setExitState();    // slot pro zmenu hodnoty, ktera oznacuje, zda se ma ukoncovaci okno zobrazovat ci nikoliv
                            // pokud je checked - nezobrazovat okno
    void setColor();    // slot pro nastaveni barvy pozadi hlavniho okna
    void reset();   // slot pro obnoveni puvodniho nastaveni na karte
    void minus();   // slot pro pridani mesta do seznamu
    void plus();    // slot pro odebrani mesta ze seznamu
};

/** trida pro kartu 2 - Nastaveni API */
class WeatherApiTab : public QWidget
{
    Q_OBJECT

    QComboBox * listOfLanguagesCombo;
    QRadioButton * rMeter;
    QRadioButton * rImperial;
    QPushButton * bReset;
    QSpinBox * spinBox;

public:
    explicit WeatherApiTab(QWidget * parent = 0);

    QString units;
    QString lang;
    int forecastLength;

    // metody pro zmenu posledniho nastaveni pred ukoncenim aplikace
    void setLangFromOut(QString setLang);
    void setUnitsFromOut(QString setUnits);
    void setForecastLengthFromOut(int setForecastLength);

    void isDefault(); // metoda pro kontrolu po spusteni aplikace, zda je nejake nastaveni jine nez vychozi, pokud ano, pak se zpristupni tlacitko reset

public slots:
    void setLang();     // slot pro nastaveni jazyka pri zmene
    void reset();   // slot pro obnoveni puvodniho nastaveni na karte
};

/** trida pro kartu 3 - Prevody jednotek */
class UnitConverterTab : public QWidget
{
    Q_OBJECT
    
    QRadioButton * rCtoF;
    QRadioButton * rFtoC;

    QLineEdit * LIn;
    QLineEdit * LOut;
    QSpinBox * spinBox;

public:
    explicit UnitConverterTab(QWidget * parent = 0);

public slots:
    void clear();   // slot pro smazani vstupu a vystupu
    void convert(); // slot pro prevod na druhou jednotku
    void clearOut();    // slot pro smazani vystupu pri prepnuti radiobuttonu
};

/** trida pro hlavni okno moznosti */
class WeatherSettingsDialog : public QDialog
{
    Q_OBJECT

    // instance jednotlivych karet
    WeatherApiTab * weatherApiTab;
    GeneralTab * generalTab;
    UnitConverterTab * unitConverterTab;

protected:
    
public:
    explicit WeatherSettingsDialog(const QString &fileName, QWidget * parent = 0);
    
    QString getUnits(); // metoda pro ziskani jednotek z venku
    QString getLang();  // metoda pro ziskani jazyka vstupu z venku
    bool getExitState();    // metoda pro ziskani stavu ukoncovaciho okna
    QVector<bool> getVisibleTime(); // metoda pro ziskani vektoru s ulozenymi stavy, ktere vyznacuji stavy viditelnosti labelu
    QStringList * getList();    // metoda pro ziskani aktualniho seznamu s ulozenymi mesty
    QString getColor(); // metoda pro ziskani aktualni barvy pozadi
    int getStyle(); // metoda pro ziskani aktualniho stylu aplikace
    int getForecastLength();    // metoda pro ziskani aktalni hodnoty pro delku predpovedi
    void setVar(QString setLang, QString setUnits); // metoda pro nastaveni poslednich ulozenych nastaveni pred ukoncenim aplikace - metoda posle instanci toto nastaveni
    void setExitMsgBox(bool setShowExitWindow); // metoda pro nastaveni posledniho ulozeneho nastaveni pred ukoncenim aplikace
    void setVisibleTime(QVector<bool> timeVectorVisible); // metoda pro nastaveni posledniho ulozeneho stavu viditelnosti pred ukoncenim aplikace
    void setListOfPlaces(QStringList setListOfPlaces); // metoda pro nastaveni posledniho ulozeneho stavu seznamu mist pred ukoncenim aplikace
    void errSave(); // metoda pro obnoveni nastaveni pri chybe pri ukladani
    void setColor(QString color);   // metoda pro nastaveni posledniho ulozeneho stavu barvy pozadi hlavniho okna pred ukoncenim aplikace
    void setStyle(int style_index); // metoda pro nastaveni posledniho ulozeneho stavu stylu pred poslednim ukoncenim aplikace
    void setForecastLength(int length); // metoda pro nastaveni posledniho ulozeneho stavz delky predpoved pred poslednim ukoncenim aplikace

    void act(); // metoda pro aktualizaci widgetu
    void setWindowNameSaveState(bool state);    // metoda pro nastaveni stavu ulozeni poslednich změt před ukončením dialogu možností

private:
    QTabWidget * tabWidget;
    QDialogButtonBox * buttonBox;

public slots:

};

/** trida pro ukoncovaci okno */
class ExitDialog : public QDialog
{
    Q_OBJECT

    QCheckBox * cAgain;
    QDialogButtonBox * buttonBox;

protected:

public:
    explicit ExitDialog(QWidget * parent = 0);

    bool exitState;
    bool checked;

public slots:
    void exitYes(); // slot pro zjisteni zmeny stavu checkBoxu
};
