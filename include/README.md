## Rozdělení souborů
Rozdělení a popis souborů je následující:
- myDefines.hpp - direktivy pro preprocesor
- weatherApi.hpp - třída pro zajištění dat získaných z API
- weatherDatabase.hpp - třída pro zajištění ukládání dat do (SQLite) databáze
- weatherForecast.hpp - třída pro zajištění grafického zobrazení předpovědi počasí
- weatherSettingsDialog.hpp - třída pro dialogová okna možností a exit dialogu
- weatherTime.hpp - třída pro zajištění funkcionality s časem
- weatherWidget.hpp - hlavní třída pro propojení GUI a funkcionalit
