#pragma once

#include <QtWidgets>

#include <QVector>
#include <QPixmap>
#include <QPalette>
#include <QColor>

/** trida pro zobrazeni predpovedi */
class ForecastDialog : public QDialog
{
    Q_OBJECT

    QLabel * lCurrentWeather;
    QLabel * lTown;
    QLabel * lTemperature;
    QLabel * lCurrentTime;
    QLabel * lDesc;

    QVector<QLabel *> lWeather;
    QVector<QLabel *> lWeatherDesc;
    QVector<QLabel *> lHours;
    QVector<QLabel *> lIcons;

    int length;

protected:
    
public:
    explicit ForecastDialog(int dataCount, QWidget * parent = 0);

    void setData(QVector<double> temp, QVector<QString> desc, QVector<QString> hours, QVector<QPixmap> icons, QString units);   // pretizena metoda pro ziskani vektoru s daty z API
    void setData(QString place, QString country, QPixmap weatherPixmap, double currentTemp, QString desc, QString units);   // pretizena metoda pro ziskani aktualnich dat
    void setData(QString time); // pretizena metoda pro ziskani casu, kdy se jednalo o aktualni informaci (aktualni pocasi)

public slots:

};