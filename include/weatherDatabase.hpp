#pragma once

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

// komponenta pro zajisteni spojeni s databazi
class WeatherDatabase : public QWidget
{
    Q_OBJECT

    QSqlDatabase db;
    unsigned int ID;

public:
    WeatherDatabase(QWidget * parent = 0);

    void saveInDatabase(QString town, QString country, double temperature, double humidity, double pressure, QString icon, unsigned int timestamp);

public slots:

};