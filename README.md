## Semestrální práce předmětu Praktické programování v C/C++ (PPC)
### Zadání
Vytvořit ucelený program, který ověří studentovy schopnosti, které postupně nabral při studiu prvních dvou semestrů (1. ročník) na ČVUT. Případně navazující na schopnosti utvořené samostudiem získané v průběhu minulosti.

### Cíl projektu
Vytvořit aplikaci, která má strukturovaný zdrojový kód - idea OOP, tzn.:
- Používání tříd a struktur
- Snaha o přehlednost kódu
- Rozdělení jednotlivých funkčních celků
- Oddělení logiky od grafického znázornění
- Vytvoření vzájemných vztahů mezi těmito funkčními celky
- Přetěžování jednotlivých komponent programu

Dále vybrané téma projektu navazuje na poslední domácí úlohu. Cílem tedy bylo vytvořit **rozšiřující** verzi verze původní.

Dalším cílem je osvojení používání systému GitLab.

### Popis složek a souborů
- dokumenty - dokumentace k projektu
- fonts - složka s uloženými speciálnějšími fonty
- icons - složka s uloženými png soubory použitými v programu
- include - složka s knihovnami (hpp soubory)
    - myDefines.hpp - direktivy pro preprocesor
    - weatherApi.hpp - třída pro zajištění dat získaných z API
    - weatherDatabase.hpp - třída pro zajištění ukládání dat do (SQLite) databáze
    - weatherForecast.hpp - třída pro zajištění grafického zobrazení předpovědi počasí
    - weatherSettingsDialog.hpp - třída pro dialogová okna možností a exit dialogu
    - weatherTime.hpp - třída pro zajištění funkcionality s časem
    - weatherWidget.hpp - hlavní třída pro propojení GUI a funkcionalit
- qss - složka se styly
- src - složka se zdrojovými kódy spojených s knihovnami (cpp soubory)
    - main.cpp - hlavní spouštěcí soubor aplikace
    - weatherApi.cpp
    - weatherDatabase.cpp
    - weatherForecast.cpp
    - weatherSettingsDialog.cpp
    - weatherTime.cpp
    - weatherWidget.cpp
- videoUkazka.mkv - video ukázka projektu
- weatherApp.pro - projektový soubor
- srcPaths.qrc - soubor cest ke zdrojovým souborům (obrázky, ...)
- weather.db - databáze
